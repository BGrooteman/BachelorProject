package com.oracle.truffle.pl.nodes.access;

import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.NodeChildren;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

@NodeInfo(shortName = ".")
@NodeChildren({@NodeChild("receiverNode"), @NodeChild("nameNode"), @NodeChild("valueNode")})
public abstract class PLWritePropertyNode extends PLExpressionNode {
	
	@Child private PLWritePropertyCacheNode writeNode = PLWritePropertyCacheNodeGen.create();
	
	@Specialization
	protected Object write(VirtualFrame frame, Object receiver, Object name, Object value) {
		writeNode.executeWrite(frame, receiver, name, value);
		return value;
	}
}