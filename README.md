# Java in Java with Truffle
Hi, my name is Bram Grooteman and this is the repository for my Bachelors project in which I implemented a small language that runs on the GraalVM.
The language is named JeSSEL and can be found under `/JeSSEL/` (accompanied by it's own README on how to use it).
The actal Thesis can be found under `Administration/Thesis'.
The PracticeLanguage folder contains a clone of the SimpleLanguage language that I wrote/copied to get used to the Truffle Framework.
