package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.LoopNode;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.pl.nodes.PLExpressionNode;
import com.oracle.truffle.pl.nodes.PLStatementNode;

@NodeInfo(shortName = "while", description = "The node implementing the while loop")
public final class PLWhileNode extends PLStatementNode {
	@Child private LoopNode loopNode;
	
	public PLWhileNode(PLExpressionNode conditionNode, PLStatementNode bodyNode){
		this.loopNode = Truffle.getRuntime().createLoopNode(new PLWhileRepeatingNode(conditionNode, bodyNode));
	}
	
	@Override
	public void setSourceSection(SourceSection section) {
		super.setSourceSection(section);
		//Propagate the sourceSection to the bodyNode
		((PLWhileRepeatingNode) loopNode.getRepeatingNode()).setSourceSection(section);
	}
	
	@Override
	public void executeVoid(VirtualFrame frame){
		loopNode.executeLoop(frame);		
	}
}
