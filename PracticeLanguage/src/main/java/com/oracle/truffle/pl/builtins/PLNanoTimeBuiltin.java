package com.oracle.truffle.pl.builtins;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;

@NodeInfo(shortName = "nanoTime")
public abstract class PLNanoTimeBuiltin extends PLBuiltinNode {
	
	@Specialization
	public long nanoTime() {
		return System.nanoTime();
	}
}