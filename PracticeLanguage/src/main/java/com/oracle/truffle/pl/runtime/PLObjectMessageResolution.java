package com.oracle.truffle.pl.runtime;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.MessageResolution;
import com.oracle.truffle.api.interop.Resolve;
import com.oracle.truffle.api.interop.UnknownIdentifierException;
import com.oracle.truffle.api.interop.java.JavaInterop;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.Node.Child;
import com.oracle.truffle.api.object.DynamicObject;
import com.oracle.truffle.pl.PLLanguage;
import com.oracle.truffle.pl.nodes.access.PLReadPropertyCacheNode;
import com.oracle.truffle.pl.nodes.access.PLReadPropertyCacheNodeGen;
import com.oracle.truffle.pl.nodes.access.PLWritePropertyCacheNode;
import com.oracle.truffle.pl.nodes.access.PLWritePropertyCacheNodeGen;
import com.oracle.truffle.pl.nodes.call.PLDispatchNode;
import com.oracle.truffle.pl.nodes.call.PLDispatchNodeGen;
import com.oracle.truffle.pl.nodes.interop.PLForeignToPLTypeNode;
import com.oracle.truffle.pl.nodes.interop.PLForeignToPLTypeNodeGen;


//Class containing all message resolution implementations of an PL object.
@MessageResolution(receiverType = PLObjectType.class, language = PLLanguage.class)
public class PLObjectMessageResolution {
	
	//Resolve WRITE message and maps it to property write access
	@Resolve(message = "WRITE")
	public abstract static class PLForeignWriteNode extends Node {
		@Child private PLWritePropertyCacheNode write = PLWritePropertyCacheNodeGen.create();
		@Child private PLForeignToPLTypeNode nameToPLType = PLForeignToPLTypeNodeGen.create();
		@Child private PLForeignToPLTypeNode valueToPLType = PLForeignToPLTypeNodeGen.create();
		
		public Object access(VirtualFrame frame, DynamicObject receiver, Object name, Object value){
			Object convertedName = nameToPLType.executeConvert(frame, name);
			Object convertedValue = nameToPLType.executeConvert(frame, value);
			write.executeWrite(frame, receiver, convertedName, convertedValue);
			return convertedValue;
		}
	}
	
	// Resolves the read message and maps it to property read access
	@Resolve(message = "READ")
	public abstract static class PLForeignReadNode extends Node {
		@Child private PLReadPropertyCacheNode read = PLReadPropertyCacheNodeGen.create();
		@Child private PLForeignToPLTypeNode nameToPLType = PLForeignToPLTypeNodeGen.create();
		
		public Object access(VirtualFrame frame, DynamicObject receiver, Object name) {
			Object convertedName = nameToPLType.executeConvert(frame, name);
			return read.executeRead(frame, receiver, convertedName);
		}
	}
	
	@Resolve(message = "INVOKE")
	public abstract static class PLForeignInvokeNode extends Node {
		@Child private PLDispatchNode dispatch = PLDispatchNodeGen.create();
		
		public Object access(VirtualFrame frame, DynamicObject receiver, String name, Object[] arguments) {
			Object property = receiver.get(name);
			if (property instanceof PLFunction) {
				PLFunction function = (PLFunction) property;
				Object[] arr = new Object[arguments.length];
				
				for (int i = 0; i < arguments.length; i++) {
					arr[i] = PLContext.fromForeignValue(arguments[i]);
				}
				Object result = dispatch.executeDispatch(frame, function, arr);
				return result;
			} else {
				throw UnknownIdentifierException.raise(name);
			}
		}
	}
	
	@Resolve(message = "KEYS")
	public abstract static class PLForeignPropertiesNode extends Node {
		public Object access(DynamicObject receiver) {
			return obtainKeys(receiver);
		}
		
		@CompilerDirectives.TruffleBoundary
		private static Object obtainKeys(DynamicObject receiver) {
			Object[] keys = receiver.getShape().getKeyList().toArray();
			return JavaInterop.asTruffleObject(keys);
		}
	}
	
}














