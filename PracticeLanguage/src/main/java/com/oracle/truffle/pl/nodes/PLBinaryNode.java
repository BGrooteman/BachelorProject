package com.oracle.truffle.pl.nodes;

import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.NodeChildren;

// Utility base class for operations that take two arguments.
// For concrete subclasses, Truffle will create two children fields 
// and the constuctors

@NodeChildren({@NodeChild("leftNode"), @NodeChild("rightNode")})
public abstract class PLBinaryNode extends PLExpressionNode {
}