package com.oracle.truffle.pl.nodes.expression;

import java.math.BigInteger;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

@NodeInfo(shortName = "const")
public final class PLBigIntegerLiteralNode extends PLExpressionNode {
	private final BigInteger value;
	
	public PLBigIntegerLiteralNode(BigInteger value) {
		this.value = value;
	}
	
	public BigInteger executeGeneric(VirtualFrame frame) {
		return value;
	}
}