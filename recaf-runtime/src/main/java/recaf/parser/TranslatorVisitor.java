package recaf.parser;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.ArrayAccessExpr;
import com.github.javaparser.ast.expr.ArrayCreationExpr;
import com.github.javaparser.ast.expr.AssignExpr;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.BooleanLiteralExpr;
import com.github.javaparser.ast.expr.CharLiteralExpr;
import com.github.javaparser.ast.expr.ConditionalExpr;
import com.github.javaparser.ast.expr.DoubleLiteralExpr;
import com.github.javaparser.ast.expr.EnclosedExpr;
import com.github.javaparser.ast.expr.IntegerLiteralExpr;
import com.github.javaparser.ast.expr.LongLiteralExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.ast.expr.ThisExpr;
import com.github.javaparser.ast.expr.UnaryExpr;
import com.github.javaparser.ast.expr.VariableDeclarationExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.BreakStmt;
import com.github.javaparser.ast.stmt.ContinueStmt;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.stmt.ForStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.ReturnStmt;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.stmt.ThrowStmt;
import com.github.javaparser.ast.stmt.WhileStmt;
import com.github.javaparser.ast.visitor.GenericVisitorAdapter;

import recaf.core.Ref;
import recaf.core.direct.Env;
import recaf.core.direct.EvalJavaStmtNoHOAS;
import recaf.core.expr.EvalJavaExpr;
import recaf.core.expr.EvalJavaExprNoHOAS;
import recaf.core.expr.EvalJavaHelper;
import recaf.core.direct.IEval;
import recaf.core.direct.IEvalEnv;
import recaf.core.direct.IExecEnv;


public class TranslatorVisitor extends GenericVisitorAdapter<Object, Object> {	
	
	
	private EvalJavaStmtNoHOAS<Object> algebraStmt;
	private EvalJavaExprNoHOAS algebraExpr;
	
	public TranslatorVisitor() {
		this.algebraStmt = new EvalJavaStmtNoHOAS<Object>(){};
		this.algebraExpr = new EvalJavaExprNoHOAS(){};
	}
	
	static IExecEnv makeExec(IExecEnv e) {return e;}
	
	@Override
	public Object visit(BlockStmt block, Object arg1) {
		IExecEnv nodes = (IExecEnv) block.getStatements().accept(this, arg1);
		return nodes;
	}
	
	// Recursive function that builds up the TODO Dit gaat fout
	private IExecEnv makeBodyFromList(List<Statement> nodeList) {
		IExecEnv result = algebraStmt.Empty();
		for (int i = 0; i < nodeList.size(); i++) {
			if (nodeList.get(i) instanceof ExpressionStmt) {
				ExpressionStmt stmt = ((ExpressionStmt) nodeList.get(i));
				if (stmt.getExpression() instanceof VariableDeclarationExpr) {
					ArrayList<Statement> newList = new ArrayList<>(nodeList.subList(i+1, nodeList.size()));
					IExecEnv body = makeBodyFromList(newList);
					return algebraStmt.Seq(result, (IExecEnv) stmt.getExpression().accept(this, body));
				}
			}
			IExecEnv newStmt = (IExecEnv) nodeList.get(i).accept(this, null);
			result = algebraStmt.Seq(result, newStmt);
		}
		return result;
	}
	
	
	@Override 
	public Object visit(@SuppressWarnings("rawtypes") NodeList nodeList, Object arg1) {
		return makeBodyFromList(nodeList);
	}

	/* ==================== Statement ====================== */
	
	@Override
	public Object visit(BreakStmt stmt, Object arg1) {
		if (stmt.getLabel().isPresent()){
			String label = stmt.getLabel().orElse(null).getIdentifier();
			return algebraStmt.Break(label);
		}
		return algebraStmt.Break();
	}
	
	@Override
	public Object visit(ContinueStmt stmt, Object arg1) {
		if (stmt.getLabel().isPresent()) {
			String label = stmt.getLabel().orElse(null).getIdentifier();
			return algebraStmt.Continue(label);
		}
		return algebraStmt.Continue();
	}
	
	@Override
	public Object visit(IfStmt stmt, Object arg1) {
		IEvalEnv condition = (IEvalEnv) stmt.getCondition().accept(this, arg1);
		IExecEnv thenPart = (IExecEnv) stmt.getThenStmt().accept(this, arg1);
		
		if (stmt.getElseStmt().isPresent()) {
			IExecEnv elsePart = (IExecEnv) stmt.getElseStmt().orElse(null).accept(this, arg1);
			
			return makeExec((l, env) -> algebraStmt.If(() -> (boolean) condition.eval(env), thenPart, elsePart).exec(l, env));
		}
		return makeExec((l, env) -> algebraStmt.If(() -> (boolean) condition.eval(env), thenPart).exec(l, env));
	}

	@Override
	public Object visit(ReturnStmt stmt, Object arg1) {
		if (stmt.getExpression().isPresent()) {
			IEvalEnv returnExpr = (IEvalEnv) stmt.getExpression().orElse(null).accept(this, arg1);			
			return makeExec((l,env) -> algebraStmt.Return(() -> returnExpr.eval(env)).exec(l, env));
		}
		return algebraStmt.Return();
	}
	
	@Override
	public Object visit(WhileStmt stmt, Object arg1) {
		
		IEvalEnv condition = (IEvalEnv) stmt.getCondition().accept(this, arg1);
		IExecEnv body = (IExecEnv) stmt.getBody().accept(this, arg1);
		return makeExec((l, env) -> algebraStmt.While(() -> {return (boolean) condition.eval(env);}, body).exec(l, env));
	}
	
//	@Override
//	public Object visit(ForStmt stmt, Object arg1) {
//		stmt.getInitialization()
//		stmt.getCompare()
//		stmt.getUpdate();
//		IExecEnv body = (IExecEnv) stmt.getBody().accept(this, arg1);
//		
//		return null;
//	}
	
	@Override
	public Object visit(ThrowStmt stmt, Object arg1) {
		IEvalEnv expr = (IEvalEnv) stmt.getExpression().accept(this, arg1);
		return makeExec((l, env) -> algebraStmt.Throw(() -> {expr.eval(env); return null; }).exec(l, env));
	}
	

	
	/* =========== Array Stuff ============== */ 

//	@Override
//	public Object visit(ArrayCreationExpr expr, Object arg1){
//		
//		if (expr.getInitializer().isPresent()) {
//			(IExecEnv) expr.getInitializer().orElse(null).accept(this, arg1);
//		}
//		
//		algebraExpr.New(clazz, args);
//		
//		return null;
//	}
//
//	@Override 
//	public Object visit(ArrayAccessExpr expr, Object arg1) {
//	// Need IEval of array (?) and IEval of index
//		IEvalEnv index = (IEvalEnv) expr.getIndex().accept(this, arg1);
//		IEvalEnv array = (IEvalEnv) expr.getName().accept(this, arg1);
//		return algebraExpr.ArrayAccess(array, index);
//	}

	
	/* ======================= Expressions ========================== */
	
	@Override
	public Object visit(EnclosedExpr expr, Object arg1) {
		if(expr.getInner().isPresent()) {
			return expr.getInner().orElse(null).accept(this, arg1);
		} else {
			return null; //TODO?
		}
	}
	
	@Override
	public Object visit(NameExpr expr, Object arg1) {
		String str = expr.getNameAsString();
		return algebraExpr.Ref(str);
	}
	
	@Override
	public Object visit(VariableDeclarator expr, Object body) {
		String target = expr.getNameAsString();
		IExecEnv bodyExec = (IExecEnv) body;
		IEvalEnv value = null;
		if(expr.getInitializer().isPresent()) {
			final IEvalEnv finalVal = (IEvalEnv) expr.getInitializer().orElse(null).accept(this, null);
			return makeExec((l, env) -> algebraStmt.Decl(target,() -> {return EvalJavaHelper.toValue(finalVal.eval(env));}, bodyExec).exec(l, env));
		}
		return makeExec((l, env) -> algebraStmt.Decl(target,() -> {return value;}, bodyExec).exec(l, env));
		
	}
	
	@Override
	public Object visit(ConditionalExpr expr, Object arg1) {
		IEvalEnv condition = (IEvalEnv) expr.getCondition().accept(this, arg1);
		IEvalEnv thenExpr = (IEvalEnv) expr.getThenExpr().accept(this, arg1);
		IEvalEnv elseExpr = (IEvalEnv) expr.getElseExpr().accept(this, arg1);
		return algebraExpr.Cond(condition, thenExpr, elseExpr);
	}
	
	@Override
	public Object visit(ExpressionStmt expr, Object arg1) {		
		IEvalEnv expression = (IEvalEnv) expr.getExpression().accept(this, arg1);
		IExecEnv ding = makeExec((l,env) -> algebraStmt.ExpStat(() -> {expression.eval(env); return null;}).exec(l, env));
		return ding;
	}
	
	@Override
	public Object visit(AssignExpr expr, Object arg1) {
		IEvalEnv left = (IEvalEnv) expr.getTarget().accept(this, arg1);
		IEvalEnv right = (IEvalEnv) expr.getValue().accept(this, arg1);
		
		String operator = expr.getOperator().asString();
		
		IEvalEnv result;
		switch (operator) {
			case "=":
				result = algebraExpr.Assign(left, right);
				break;
			case "<<=":
				result = algebraExpr.AssignLeftShift(left, right);
				break;
			case ">>=":
				result = algebraExpr.AssignRightShift(left, right);
				break;
			case "|=":
				result = algebraExpr.AssignOr(left, right);
				break;
			case "&=":
				result = algebraExpr.AssignAnd(left, right);
				break;
			case "%=":
				result = algebraExpr.AssignRemain(left, right);
				break;
			case "+=":
				result = algebraExpr.AssignPlus(left, right);
				break;
			case "^=":
				result = algebraExpr.AssignExcOr(left, right);
				break;
			case "/=":
				result = algebraExpr.AssignDiv(left, right);
				break;
			case ">>>=":
				result = algebraExpr.AssignURightShift(left, right);
				break;
			case "-=":
				result = algebraExpr.AssignMinus(left, right);
				break;
			default:
				throw new UnsupportedOperationException("Dont know what to do with operator: " + operator);
		}
		return result;
	}
	
	@Override
	public Object visit(BinaryExpr expr, Object arg1) {
		IEvalEnv left = (IEvalEnv) expr.getLeft().accept(this, arg1);
		IEvalEnv right = (IEvalEnv) expr.getRight().accept(this, arg1);
		String operator = expr.getOperator().asString();
		IEvalEnv result;
		
    	switch (operator) {
	        case "+":
	            result = algebraExpr.Plus(left, right);
	            break;
	        case "*":
	            result = algebraExpr.Mul(left, right);
	            break;
	        case "/":
	            result = algebraExpr.Div(left, right);
	            break;
	        case "-":
	            result = algebraExpr.Minus(left, right);
	            break;
	        case "%":
	            result = algebraExpr.Remain(left, right);
	            break;
	        case "<":
	            result = algebraExpr.Lt(left, right);
	            break;
	        case "<=":
	            result = algebraExpr.LtEq(left, right);
	            break;
	        case ">":
	            result = algebraExpr.Gt(left, right);
	            break;
	        case ">=":
	            result = algebraExpr.GtEq(left, right);
	            break;
	        case "==":
	            result = algebraExpr.Eq(left, right);
	            break;
	        case "!=":
	            result = algebraExpr.NotEq(left, right);
	            break;
	        case "&&":
	            result = algebraExpr.LazyAnd(left, right);
	            break;
	        case "||":
	            result = algebraExpr.LazyOr(left, right);
	            break;
	        case "<<":
	            result = algebraExpr.LeftShift(left, right);
	            break;
	        case ">>":
	            result = algebraExpr.RightShift(left, right);
	            break;
	        case ">>>":
	        	result = algebraExpr.URightShift(left, right);
	        	break;
	        case "|":
	            result = algebraExpr.Or(left, right);
	            break; 
	        case "^":
	            result = algebraExpr.ExcOr(left, right);
	            break; 
	        case "&":
	            result = algebraExpr.And(left, right);
	            break;
	           
	        default:
	        	throw new UnsupportedOperationException("Don't know what to do with operator: " + operator);
    	}
    	return result;
	}
	
	@Override
	public Object visit(UnaryExpr expr, Object arg1) {
		String operator = expr.getOperator().asString();
		IEvalEnv value = (IEvalEnv) expr.getExpression().accept(this, arg1);
		
		//TODO PostInc and PreInc are same currently. Same for PostDecr and PostInc
		IEvalEnv result;
		switch (operator) {
		case "++":
			result = algebraExpr.PreIncr(value);
			break;
		case "--":
			result = algebraExpr.PreDecr(value);
			break;
		case "+":
			result = algebraExpr.Mul(algebraExpr.Lit(1), value); // Nothing
			break;
		case "-":
			result = algebraExpr.Minus(value);
			break;
		case "!":
			result = algebraExpr.Not(value);
			break;
		default:
			throw new UnsupportedOperationException("Operator is not supported " + operator);
		}
		return result;
	}
	
	/*============================= Literals ========================*/ 

	
	@Override
	public Object visit(DoubleLiteralExpr literal, Object arg1) {
		return algebraExpr.Lit(Double.parseDouble(literal.getValue()));
	}
	
	@Override
	public Object visit(LongLiteralExpr literal, Object arg1) {
		return algebraExpr.Lit(Long.parseLong(literal.getValue()));
	}
	

	@Override
	public Object visit(IntegerLiteralExpr literal, Object arg1) {
		return algebraExpr.Lit(Integer.parseInt(literal.getValue()));
	}
	
	@Override
	public Object visit(CharLiteralExpr literal, Object arg1) {
		return algebraExpr.Lit(literal.getValue());
	}
	
	@Override
	public Object visit(StringLiteralExpr literal, Object arg1) {
		return algebraExpr.Lit(literal.getValue());
	}
	
	@Override
	public Object visit(BooleanLiteralExpr literal, Object arg1) {
		return algebraExpr.Lit(literal.getValue());
	}
	
	
}