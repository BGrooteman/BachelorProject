package com.oracle.truffle.pl.nodes.expression;

import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

@NodeInfo(shortName = "||")
public final class PLLogicalOrNode extends PLShortCircuitNode {
	
	public PLLogicalOrNode(PLExpressionNode left, PLExpressionNode right) {
		super(left, right);
	}
	
	@Override
	protected boolean isEvaluateRight(boolean leftValue) {
		return !leftValue;
	}
	
	@Override
	protected boolean execute(boolean left, boolean right) {
		return left || right;
	}
}