package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLExpressionNode;
import com.oracle.truffle.pl.nodes.PLStatementNode;
import com.oracle.truffle.pl.runtime.PLNull;

@NodeInfo(shortName = "return", description = "The node implementing a return statement")
public final class PLReturnNode extends PLStatementNode {
	
	@Child private PLExpressionNode valueNode;
	
	public PLReturnNode(PLExpressionNode valueNode){
		this.valueNode = valueNode;
	}
	
	@Override
	public void executeVoid(VirtualFrame frame){
		Object result;
		if (valueNode != null) {
			result = valueNode.executeGeneric(frame);
		} else {
			result = PLNull.SINGLETON;
		}
		throw new PLReturnException(result);
	}
}