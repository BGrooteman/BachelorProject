package com.BScProject.truffle.jsl.runtime;

import com.BScProject.truffle.jsl.nodes.call.JSLDispatchNode;
import com.BScProject.truffle.jsl.nodes.call.JSLDispatchNodeGen;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.CanResolve;
import com.oracle.truffle.api.interop.MessageResolution;
import com.oracle.truffle.api.interop.Resolve;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.nodes.Node;
import com.BScProject.truffle.jsl.runtime.JSLContext;
import com.BScProject.truffle.jsl.JSLLanguage;

/**
 * The class containing all message resolution implementations of {@link SLFunction}.
 */
@MessageResolution(receiverType = JSLFunction.class, language = JSLLanguage.class)
public class JSLFunctionMessageResolution {
	
	@Resolve(message = "EXECUTE")
	public abstract static class ForeignFunctionExecuteNode extends Node {
		
		@Child private JSLDispatchNode dispatchNode = JSLDispatchNodeGen.create();
		
		public Object access(VirtualFrame frame, JSLFunction receiver, Object[] arguments) {
			Object[] arr = new Object[arguments.length];
			
			for(int i = 0; i < arr.length; i++) {
				arr[i] = JSLContext.fromForeignValue(arguments[i]);
			}
			Object result = dispatchNode.executeDispatch(frame, receiver, arr);
			return result;
		}
	}
	
	@Resolve(message = "IS_EXECUTABLE")
	public abstract static class JSLForeignIsExecutableNode extends Node {
		public Object access(Object receiver) {
			return receiver instanceof JSLFunction;
		}
	}
	
	@CanResolve
	public abstract static class CheckFunction extends Node {
		protected static boolean test(TruffleObject receiver) {
			return receiver instanceof JSLFunction;
		}
	}
	
}