package com.oracle.truffle.pl.builtins;

import com.oracle.truffle.api.dsl.Cached;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;

@NodeInfo(shortName = "import")
@SuppressWarnings("unused")
public abstract class PLImportBuiltin extends PLBuiltinNode {
	
	@Specialization(guards = "stringsEqual(cachedName, name)")
	public Object importSymbol(String name,
					@Cached("name") String cachedName,
					@Cached("doImport(name)") Object symbol) {
		return symbol;
	}
	
	protected Object doImport(String name) {
		return getContext().importSymbol(name);
	}
	
	protected static boolean stringsEqual(String a, String b) {
		return a.equals(b);
	}
}