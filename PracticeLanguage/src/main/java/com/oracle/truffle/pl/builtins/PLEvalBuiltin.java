package com.oracle.truffle.pl.builtins;

import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Cached;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.DirectCallNode;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.Source;

@NodeInfo(shortName = "eval")
@SuppressWarnings("unused")
public abstract class PLEvalBuiltin extends PLBuiltinNode {
	
	@Specialization(guards = {"stringsEqual(cachedMimeType, mimeType)", "stringsEqual(cachedCode, code)"})
	public Object evalCached(VirtualFrame frame, String mimeType, String code,
					@Cached("mimeType") String cachedMimeType,
					@Cached("code") String cachedCode,
					@Cached("create(parse(mimeType, code))") DirectCallNode callNode) {
		return callNode.call(frame, new Object[]{});
	}
	
	@TruffleBoundary
	@Specialization(replaces = "evalCached")
	public Object evalUncached(String mimeType, String code) {
		return parse(mimeType, code).call();
	}
	
	protected CallTarget parse(String mimeType, String code) {
		final Source source = Source.newBuilder(code).name("(eval)").mimeType(mimeType).build();
		
		try {
			return getContext().parse(source);
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
	}
	
	// Work around findbugs warning (?)
	protected static boolean stringsEqual(String a, String b) {
		return a.equals(b);
	}
	
	
	
	
	
}