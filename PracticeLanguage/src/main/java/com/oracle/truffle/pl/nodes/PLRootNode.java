package com.oracle.truffle.pl.nodes;

import com.oracle.truffle.api.CompilerDirectives.CompilationFinal;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.nodes.RootNode;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.pl.PLLanguage;

@NodeInfo(language = "PL", description = "The root of all PL execution trees")
public class PLRootNode extends RootNode {

	@Child private PLExpressionNode bodyNode;
	
	private final String name;
	
	@CompilationFinal private boolean isCloningAllowed;
	
	public PLRootNode(FrameDescriptor frameDescriptor, PLExpressionNode bodyNode, SourceSection sourceSection,
			String name) {
		super(PLLanguage.class, sourceSection, frameDescriptor);
		this.bodyNode = bodyNode;
		this.name = name;
	}
	
	@Override
	public Object execute(VirtualFrame frame) {
		assert PLLanguage.INSTANCE.findContext() != null;
		return bodyNode.executeGeneric(frame);
	}
	
	public PLExpressionNode getBodyNode() {
		return bodyNode;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	public void setCloningAllowed(boolean isCloningAllowed) {
		this.isCloningAllowed = isCloningAllowed;
	}
	
	@Override
	public boolean isCloningAllowed() {
		return isCloningAllowed;
	}
	
	@Override
	public String toString() {
		return "root" + name;
	}
	
}
