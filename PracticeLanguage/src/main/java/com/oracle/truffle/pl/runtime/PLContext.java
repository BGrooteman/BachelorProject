package com.oracle.truffle.pl.runtime;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.math.BigInteger;

import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.ExecutionContext;
import com.oracle.truffle.api.TruffleLanguage;
import com.oracle.truffle.api.dsl.NodeFactory;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.nodes.RootNode;
import com.oracle.truffle.api.object.DynamicObject;
import com.oracle.truffle.api.object.Layout;
import com.oracle.truffle.api.object.Shape;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.pl.PLLanguage;
import com.oracle.truffle.pl.builtins.PLBuiltinNode;
import com.oracle.truffle.pl.builtins.PLDefineFunctionBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLEvalBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLGetSizeBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLHasSizeBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLHelloEqualsWorldBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLImportBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLIsExecutableBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLIsNullBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLNanoTimeBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLNewObjectBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLPrintlnBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLReadlnBuiltinFactory;
import com.oracle.truffle.pl.builtins.PLStackTraceBuiltinFactory;
import com.oracle.truffle.pl.nodes.PLExpressionNode;
import com.oracle.truffle.pl.nodes.PLRootNode;
import com.oracle.truffle.pl.nodes.local.PLReadArgumentNode;

/* The run-time state of PL during execution. 
 * Created in the PLLanguage
 */

public final class PLContext extends ExecutionContext {
	
	private static final Source BUILTIN_SOURCE = Source.newBuilder("").name("PL builtin").mimeType(PLLanguage.MIME_TYPE).build();
	private static final Layout LAYOUT = Layout.createLayout();
	
	private final BufferedReader input;
	private final PrintWriter output;
	private final PLFunctionRegistry functionRegistry;
	private final Shape emptyShape;
	private final TruffleLanguage.Env env;
	
	public PLContext(TruffleLanguage.Env env, BufferedReader input, PrintWriter output){
		this.input = input;
		this.output = output;
		this.env = env;
		this.functionRegistry = new PLFunctionRegistry();
		installBuiltins();
		
		this.emptyShape = LAYOUT.createShape(PLObjectType.SINGLETON);
	}
	
	public BufferedReader getInput() {
		return input;
	}
	
	public PrintWriter getOutput() {
		return output;
	}
	
	public PLFunctionRegistry getFunctionRegistry(){
		return functionRegistry; 
	}
	
	private void installBuiltins() {
		installBuiltin(PLDefineFunctionBuiltinFactory.getInstance());
		installBuiltin(PLEvalBuiltinFactory.getInstance());
		installBuiltin(PLGetSizeBuiltinFactory.getInstance());
		installBuiltin(PLHasSizeBuiltinFactory.getInstance());
		installBuiltin(PLHelloEqualsWorldBuiltinFactory.getInstance());
		installBuiltin(PLImportBuiltinFactory.getInstance());
		installBuiltin(PLIsExecutableBuiltinFactory.getInstance());
		installBuiltin(PLIsNullBuiltinFactory.getInstance());
		installBuiltin(PLNanoTimeBuiltinFactory.getInstance());
		installBuiltin(PLNewObjectBuiltinFactory.getInstance());
		installBuiltin(PLPrintlnBuiltinFactory.getInstance());
		installBuiltin(PLReadlnBuiltinFactory.getInstance());
		installBuiltin(PLStackTraceBuiltinFactory.getInstance());
	}
	
	private void installBuiltin(NodeFactory<? extends PLBuiltinNode> factory) {
		//These node factories are builtin 
		int argumentCount = factory.getExecutionSignature().size();
		PLExpressionNode[] argumentNodes = new PLExpressionNode[argumentCount];
		
		for(int i = 0; i < argumentCount; i++) {
			argumentNodes[i] = new PLReadArgumentNode(i);
		}
		
		PLBuiltinNode builtinBodyNode = factory.createNode(argumentNodes, this);
		builtinBodyNode.addRootTag();
		
		String name = lookupNodeInfo(builtinBodyNode.getClass()).shortName();
		final SourceSection srcSection = BUILTIN_SOURCE.createUnavailableSection();
		builtinBodyNode.setSourceSection(srcSection);
		
		PLRootNode rootNode = new PLRootNode(new FrameDescriptor(), builtinBodyNode, srcSection, name);
		
		getFunctionRegistry().register(name, rootNode);
	}
	
	public static NodeInfo lookupNodeInfo(Class<?> clazz){
		if (clazz == null) {
			return null;
		}
		NodeInfo info = clazz.getAnnotation(NodeInfo.class);
		if (info == null) {
			return info;
		} else {
			return lookupNodeInfo(clazz.getSuperclass());
		}
	}
	
	// Methods for Object creation
	
	// All objects start off without properties. Storing properties triggers change of shape.
	public DynamicObject createObject() {
		return emptyShape.newInstance();
	}
	
	public static boolean isPLObject(TruffleObject value) {
		//LAYOUT.getType() returns concrete implementation of class.
		// This makes it faster than just checking for the DynamicObject base class.
		return LAYOUT.getType().isInstance(value) && LAYOUT.getType().cast(value).getShape().getObjectType() == PLObjectType.SINGLETON;
	}
	
	
	//Methods for language interoperability
	
	public static Object fromForeignValue(Object a) {
		if (a instanceof Long || a instanceof BigInteger || a instanceof String) {
			return a;
		} else if (a instanceof Number) {
			return fromForeignNumber(a);
		} else if (a instanceof TruffleObject) {
			return a;
		} else if (a instanceof PLContext) {
			return a;
		}
		CompilerDirectives.transferToInterpreter();
		throw new IllegalStateException(a + " is not a Truffle value");
	}
	
	
	@TruffleBoundary
	private static long fromForeignNumber(Object a) {
		return ((Number) a).longValue();
	}
	
	public CallTarget parse(Source source) throws Exception {
		return env.parse(source);
	}
	
	
	//Tries to find an imported symbol from the registered languages in the envirnoment,
	//returns TruffleObject, a wrapper of primitive type or null. 
	@TruffleBoundary
	public Object importSymbol(String name) {
		Object object = env.importSymbol(name);
		Object plValue = fromForeignValue(object);
		return plValue;
	}
	
	
	
		
	
	
	
}