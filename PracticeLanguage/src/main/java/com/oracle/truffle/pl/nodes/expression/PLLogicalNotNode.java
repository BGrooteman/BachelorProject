package com.oracle.truffle.pl.nodes.expression;

import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

@NodeChild("valueNode")
@NodeInfo(shortName = "!")
public abstract class PLLogicalNotNode extends PLExpressionNode {
	
	@Specialization
	protected boolean doBoolean(boolean value) {
		return !value;
	}
}