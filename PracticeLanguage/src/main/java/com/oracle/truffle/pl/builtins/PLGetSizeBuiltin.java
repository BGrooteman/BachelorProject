package com.oracle.truffle.pl.builtins;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.Message;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.interop.UnsupportedMessageException;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.PLException;

@NodeInfo(shortName = "getSize")
public abstract class PLGetSizeBuiltin extends PLBuiltinNode {
	@Child private Node getSize = Message.GET_SIZE.createNode();
	
	@Specialization
	public Object getSize(VirtualFrame frame, TruffleObject obj) {
		try {
			return ForeignAccess.sendGetSize(getSize, frame, obj);
		} catch (UnsupportedMessageException e) {
			throw new PLException(e.getMessage());
		}
	}
}