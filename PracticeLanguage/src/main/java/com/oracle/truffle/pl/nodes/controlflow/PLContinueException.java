package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.nodes.ControlFlowException;

public final class PLContinueException extends ControlFlowException {
	
	private static final long serialVersionUID = 1L;
	public static final PLContinueException INSTANCE = new PLContinueException();
	
	private PLContinueException(){
	}
	
}