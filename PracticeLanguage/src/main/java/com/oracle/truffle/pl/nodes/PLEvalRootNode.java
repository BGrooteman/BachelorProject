package com.oracle.truffle.pl.nodes;

import java.util.Map;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.CompilerDirectives.CompilationFinal;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.pl.PLLanguage;
import com.oracle.truffle.pl.runtime.PLContext;

public final class PLEvalRootNode extends PLRootNode {
	private final Map<String, PLRootNode> functions;
	@CompilationFinal private PLContext context;
	
	public PLEvalRootNode(FrameDescriptor frameDescriptor, PLExpressionNode bodyNode, SourceSection sourceSection,
			String name, Map<String, PLRootNode> functions) {
		super(frameDescriptor, bodyNode, sourceSection, name);
		this.functions = functions;
	}
	
	@Override
	public Object execute(VirtualFrame frame) {
		// On first execution, lazily register
		if(context == null) {
			// Registration should not be compiled apparently
			CompilerDirectives.transferToInterpreterAndInvalidate();
			
			context = PLLanguage.INSTANCE.findContext();
			context.getFunctionRegistry().register(functions);
		}
		
		if (getBodyNode() == null) {
			// Source did not have main funcrion, noting executed
			return null;
		}
		
		// Conversion of types
		Object[] arguments = frame.getArguments();
		for (int i = 0; i < arguments.length; i++) {
			arguments[i] = PLContext.fromForeignValue(arguments[i]);
		}
		
		return super.execute(frame);
		
	}
}