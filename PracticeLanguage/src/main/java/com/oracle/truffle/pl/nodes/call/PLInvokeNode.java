package com.oracle.truffle.pl.nodes.call;

import com.oracle.truffle.api.CompilerAsserts;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.instrumentation.StandardTags;
import com.oracle.truffle.api.nodes.ExplodeLoop;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

@NodeInfo(shortName = "invoke")
public final class PLInvokeNode extends PLExpressionNode {
	
	@Child private PLExpressionNode functionNode;
	@Children private final PLExpressionNode[] argumentNodes;
	@Child private PLDispatchNode dispatchNode;
	
	public PLInvokeNode(PLExpressionNode functionNode, PLExpressionNode[] argumentNodes) {
		this.functionNode = functionNode;
		this.argumentNodes = argumentNodes;
		this.dispatchNode = PLDispatchNodeGen.create();
	}
	
	@ExplodeLoop
	@Override
	public Object executeGeneric(VirtualFrame frame) {
		Object function = functionNode.executeGeneric(frame);
		
		// The number of arguments is constant for one invoke node. Therefore loop is unrolled
		CompilerAsserts.compilationConstant(argumentNodes.length);
		
		Object[] argumentValues = new Object[argumentNodes.length];
		for(int i = 0; i < argumentNodes.length; i++){
			argumentValues[i] = argumentNodes[i].executeGeneric(frame);
		}
		return dispatchNode.executeDispatch(frame, function, argumentValues);
	}
	
	@Override
	protected boolean isTaggedWith(Class<?> tag) {
		if (tag == StandardTags.CallTag.class) {
			return true;
		}
		return super.isTaggedWith(tag);
	}
}