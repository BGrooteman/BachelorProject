package com.oracle.truffle.pl.runtime;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.CanResolve;
import com.oracle.truffle.api.interop.MessageResolution;
import com.oracle.truffle.api.interop.Resolve;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.pl.PLLanguage;
import com.oracle.truffle.pl.nodes.call.PLDispatchNode;
import com.oracle.truffle.pl.nodes.call.PLDispatchNodeGen;

@MessageResolution(receiverType = PLFunction.class, language = PLLanguage.class)
public class PLFunctionMessageResolution {
	
	// An PL function resolves an EXECUTE function
	@Resolve(message = "EXECUTE")
	public abstract static class PLForeignFunctionExecteNode extends Node {
		
		@Child private PLDispatchNode dispatch = PLDispatchNodeGen.create();
		
		public Object access(VirtualFrame frame, PLFunction receiver, Object[] arguments) {
			Object[] arr = new Object[arguments.length];
			// Before the arguments can be used, they need to be converted
			for(int i = 0; i < arr.length; i++) {
				arr[i] = PLContext.fromForeignValue(arguments[i]);
			}
			Object result = dispatch.executeDispatch(frame, receiver, arr);
			return result;
		}
	}
	
	@Resolve(message = "IS_EXECUTABLE")
	public abstract static class PLForeignIsExecutableNode extends Node {
		public Object access(Object receiver) {
			return receiver instanceof PLFunction;
		}
	}
	
	@CanResolve
	public abstract static class CheckFunction extends Node {
		protected static boolean test(TruffleObject receiver) {
			return receiver instanceof PLFunction;
		}
	}
}