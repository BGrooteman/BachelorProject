package Benchmarks;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * A class that holds all the run values and file locations for the benchmarks
 * 
 */
public class BenchmarkData {
	public static abstract class Benchmark {
		private String name;
		private Path filePath;
		private List<Integer> runValues; // List of longs, since JeSSEL does not know Integers
		
		private int nrRuns;
		private int nrArguments; // Needs to be known to run the benchmark properly
		private List<String> argumentNames;
		
		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public int getNrRuns() {
			return nrRuns;
		}
		
		public void setNrRuns(int nrRuns) {
			this.nrRuns = nrRuns;
		}
		
		public int getNrArguments() {
			return nrArguments;
		}
		
		public void setNrArguments(int nrArguments) {
			this.nrArguments = nrArguments;
		}
		
		public Path getFilePath() {
			return filePath;
		}
		
		public void setFilePath(Path filePath) {
			this.filePath = filePath;
		}
		
		public List<Integer> getRunValues() {
			return runValues;
		}
		
		public void setRunValues(List<Integer> runValues) {
			this.runValues = runValues;
		}

		public List<String> getArgumentNames() {
			return argumentNames;
		}

		public void setArgumentNames(List<String> argumentNames) {
			this.argumentNames = argumentNames;
		}
	}
	
	public static class FactorCounter extends Benchmark {
		public FactorCounter(){
			setName("FactorCounter");
			setFilePath(new File("./Benchmarks/FactorCounter.jsl").toPath());
			Integer[] runs = {2500, 5000, 7500, 10000};
			String[] argNames = {"size"};
			
			setNrRuns(4);
			setNrArguments(1);
			setRunValues(Arrays.asList(runs));
			setArgumentNames(Arrays.asList(argNames));
		}
	}
	
	public static class Factorial extends Benchmark {
		public Factorial() {
			setName("Factorial");
			setFilePath(new File("./Benchmarks/Factorial.jsl").toPath());
			Integer[] runs = {10000, 20, 10000, 25, 10000, 30, 10000, 35};
			String[] argNames = {"size", "factorial"};
			
			setNrRuns(4);
			setNrArguments(2);
			setRunValues(Arrays.asList(runs));
			setArgumentNames(Arrays.asList(argNames));
		}
	}
	
	public static class Fibonacci extends Benchmark {
		public Fibonacci() {
			setName("Fibonacci");
			setFilePath(new File("./Benchmarks/Fibonacci.jsl").toPath());
			Integer[] runs = {10000, 40, 0, 1, 10000, 80, 0, 1, 10000, 120, 0, 1, 10000, 160, 0, 1};
			String[] argNames = {"size", "fib", "n1", "n2"};
		
			setNrRuns(4);
			setNrArguments(4);
			setRunValues(Arrays.asList(runs));
			setArgumentNames(Arrays.asList(argNames));
		}
		
	}
	
	public static class GCD extends Benchmark {
		public GCD(){
			setName("GCD");
			setFilePath(new File("./Benchmarks/GCD.jsl").toPath());
			Integer[] runs = {1, 1000, 82, 1, 10000, 683, 1, 50000, 217, 1, 100000, 6917};
			String[] argNames = {"r1", "r2", "k"};
			
			setNrRuns(4);
			setNrArguments(3);
			setRunValues(Arrays.asList(runs));
			setArgumentNames(Arrays.asList(argNames));
		}
	}
	
	public static class PrimeFinder extends Benchmark {
		public PrimeFinder() {
			setName("PrimeFinder");
			setFilePath(new File("./Benchmarks/PrimeFinder.jsl").toPath());
			Integer[] runs = {1000, 5000, 10000, 50000};
			String[] argNames = {"size"};
			
			setNrRuns(4);
			setNrArguments(1);
			setRunValues(Arrays.asList(runs));
			setArgumentNames(Arrays.asList(argNames));
		}
	}
	
	public static class While extends Benchmark {
		public While() {
			setName("While");
			setFilePath(new File("./Benchmarks/While.jsl").toPath());
			Integer[] runs = {500, 1000, 2500, 5000};
			String[] argNames = {"size"};
			
			setNrRuns(4);
			setNrArguments(1);
			setRunValues(Arrays.asList(runs));
			setArgumentNames(Arrays.asList(argNames));
		}
	}
	
	public static ArrayList<Benchmark> getBenchmarks() {
		ArrayList<Benchmark> benchmarkList = new ArrayList<>();
		
		benchmarkList.add(new FactorCounter());
		benchmarkList.add(new Factorial());
		 benchmarkList.add(new Fibonacci());
		 benchmarkList.add(new GCD());
		 benchmarkList.add(new PrimeFinder());
		 benchmarkList.add(new While());
		
		return benchmarkList;
		
	}
}
