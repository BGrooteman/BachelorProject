package com.oracle.truffle.pl.nodes;

import com.oracle.truffle.api.dsl.TypeSystemReference;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.instrumentation.Instrumentable;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.nodes.UnexpectedResultException;

@TypeSystemReference(PLTypes.class)
@NodeInfo(description = "The abstract base node for all expressions")
@Instrumentable(factory = PLExpressionNodeWrapper.class)
public abstract class PLExpressionNode extends PLStatementNode {
	
	// Execution when no specialization is possible. Most generic case.
	public abstract Object executeGeneric(VirtualFrame frame);
	
	// Execution when SLStatementNode is already Sufficient, return val is discarded
	@Override
	public void executeVoid(VirtualFrame frame){
		executeGeneric(frame);
	}
	
	// Specialized executions => Call the generic execution and expect the appropiate 
	// type. Type-specialized subclasses overwrite the methods (when needed).
	public long executeLong(VirtualFrame frame) throws UnexpectedResultException {
		return PLTypesGen.expectLong(executeGeneric(frame));
	}
	
	public boolean executeBoolean(VirtualFrame frame) throws UnexpectedResultException {
		return PLTypesGen.expectBoolean(executeGeneric(frame));
	}

}
