package com.BScProject.truffle.jsl.parser;


public class JSLParseException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public JSLParseException(String message) {
		super(message);
	}
}