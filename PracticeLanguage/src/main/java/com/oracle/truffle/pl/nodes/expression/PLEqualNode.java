package com.oracle.truffle.pl.nodes.expression;

import java.math.BigInteger;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLBinaryNode;
import com.oracle.truffle.pl.runtime.PLFunction;
import com.oracle.truffle.pl.runtime.PLNull;

@NodeInfo(shortName = "==")
public abstract class PLEqualNode extends PLBinaryNode {
	
	@Specialization
	protected boolean equal(long left, long right) {
		return left == right;
	}
	
	@Specialization
	@TruffleBoundary
	protected boolean equal(BigInteger left, BigInteger right) {
		return left.equals(right);
	}
	
	@Specialization
	protected boolean equal(boolean left, boolean right) {
		return left == right;
	}
	
	@Specialization
	protected boolean equal(String left, String right){
		return left.equals(right);
	}
	
	@Specialization
	protected boolean equal(PLFunction left, PLFunction right) {
		return left == right;
	}
	
	@Specialization
	protected boolean equal(PLNull left, PLNull right){
		return left == right;
	}
	
	//Guard for correctness!
	@Specialization(guards = "left.getClass() != right.getClass()")
	protected boolean equal(Object left, Object right){
		return false;
	}
}