package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLStatementNode;

@NodeInfo(shortName = "break", description = "The node implementing a break statement")
public final class PLBreakNode extends PLStatementNode {
	@Override 
	public void executeVoid(VirtualFrame frame) {
		throw PLBreakException.SINGLETON;
	}
}