package com.oracle.truffle.pl.nodes.expression;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

//Denotes parenthesized expression
@NodeInfo(description = "A parenthesized expression")
public class PLParenExpressionNode extends PLExpressionNode {
	@Child private PLExpressionNode expression;
	
	public PLParenExpressionNode(PLExpressionNode expression) {
		this.expression = expression;
	}
	
	@Override
	public Object executeGeneric(VirtualFrame frame) {
		return expression.executeGeneric(frame);
	}
	
	@Override 
	public long executeLong(VirtualFrame frame) throws UnexpectedResultException {
		return expression.executeLong(frame);
	}
	
	@Override
	public boolean executeBoolean(VirtualFrame frame) throws UnexpectedResultException {
		return expression.executeBoolean(frame);
	}
}