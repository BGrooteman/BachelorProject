package com.oracle.truffle.pl.runtime;

import com.oracle.truffle.api.Assumption;
import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.utilities.CyclicAssumption;
import com.oracle.truffle.pl.nodes.PLUndefinedFunctionRootNode;

public final class PLFunction implements TruffleObject {
	
	private final String name;
	
	private RootCallTarget callTarget;
	
	/** 
	 * Manages the assumption that callTarget is stable (not redefined in code).
	 * We use a truffle utility class that automatically creates new link when old one
	 * gets invalidated
	 */
	private final CyclicAssumption callTargetStable;
	
	protected PLFunction(String name){
		this.name = name;
		this.callTarget = Truffle.getRuntime().createCallTarget(new PLUndefinedFunctionRootNode(name));
		this.callTargetStable = new CyclicAssumption(name);
	}
	
	public String getName() {
		return name;
	}
	
	protected void setCallTarget(RootCallTarget callTarget) {
		this.callTarget = callTarget;
		//We had an assumption, invalidate it.
		callTargetStable.invalidate();
	}
	
	public RootCallTarget getCallTarget() {
		return callTarget;
	}
	
	public Assumption getCallTargetStable() {
		return callTargetStable.getAssumption();
	}
	
	// This toString is used when function literal is used in string concat.
	@Override
	public String toString() {
		return name;
	}
	
	// For links with other languages
	@Override
	public ForeignAccess getForeignAccess() {
		return PLFunctionMessageResolutionForeign.createAccess();
	}
	
}