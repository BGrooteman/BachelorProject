package com.oracle.truffle.pl.builtins;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;

@NodeInfo(shortName = "new")
public abstract class PLNewObjectBuiltin extends PLBuiltinNode {
	
	@Specialization
	public Object newObject() {
		return getContext().createObject();
	}
}