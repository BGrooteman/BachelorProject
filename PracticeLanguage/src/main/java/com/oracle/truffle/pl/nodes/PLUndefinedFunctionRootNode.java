package com.oracle.truffle.pl.nodes;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.pl.runtime.PLUndefinedNameException;

// Function nodes start off as this, when trying to execute when still undefined, it
// throws an exception
public class PLUndefinedFunctionRootNode extends PLRootNode {
	public PLUndefinedFunctionRootNode(String name) {
		super(null, null, null, name);
	}
	
	@Override
	public Object execute(VirtualFrame frame) {
		throw PLUndefinedNameException.undefinedFunction(getName());
	}
}