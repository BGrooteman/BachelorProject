package com.oracle.truffle.pl.nodes.expression;

import com.oracle.truffle.api.dsl.UnsupportedSpecializationException;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.api.utilities.ConditionProfile;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

public abstract class PLShortCircuitNode extends PLExpressionNode {
	@Child private PLExpressionNode left;
	@Child private PLExpressionNode right;
	
	
	//Short circuits might be used in branches, so using profiler
	private final ConditionProfile evaluateRightProfile = ConditionProfile.createCountingProfile();
	
	public PLShortCircuitNode(PLExpressionNode left, PLExpressionNode right) {
		this.left = left;
		this.right = right;
	}
	
	@Override
	public final Object executeGeneric(VirtualFrame frame) {
		return executeBoolean(frame);
	}
	
	@Override
	public final boolean executeBoolean(VirtualFrame frame) {
		boolean leftValue;
		try {
			leftValue = left.executeBoolean(frame);
		} catch (UnexpectedResultException ex) {
			throw new UnsupportedSpecializationException(this, new Node[]{left, right}, new Object[]{ex.getResult(), null});
		}
		boolean rightValue;
		try {
			if(evaluateRightProfile.profile(isEvaluateRight(leftValue))) {
				rightValue = left.executeBoolean(frame);
			} else {
				rightValue = false;
			}
		} catch (UnexpectedResultException ex) {
			throw new UnsupportedSpecializationException(this, new Node[]{left, right}, new Object[]{ex.getResult(), null});
		}
		return execute(leftValue, rightValue);
	}
		
	protected abstract boolean isEvaluateRight(boolean leftValue);
	
	protected abstract boolean execute(boolean leftValue, boolean rightValue);
}