package Benchmarks;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import com.BScProject.truffle.jsl.JSLException;
import com.BScProject.truffle.jsl.JSLLanguage;
import com.BScProject.truffle.jsl.parser.JSLParser;
import com.BScProject.truffle.jsl.runtime.JSLContext;
import com.BScProject.truffle.jsl.runtime.JSLFunction;
import com.oracle.truffle.api.dsl.UnsupportedSpecializationException;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.vm.PolyglotEngine;

import Benchmarks.BenchmarkData.Benchmark;

public class JeSSELBenchmarkRunner {
	
	// Setup the benchmark => Parse the source
	private static JSLFunction parseSource(PolyglotEngine engine, Benchmark benchmark){		
		// Get parsed source
		JSLContext context = (JSLContext) engine.getLanguages().get(JSLLanguage.MIME_TYPE).getGlobalObject().get();
		try {
			Path path = benchmark.getFilePath();
			Source source = Source.newBuilder(path.toFile()).mimeType(JSLLanguage.MIME_TYPE).build();
			context.getFunctionRegistry().register(JSLParser.parse(source));
	        JSLFunction mainFunction = context.getFunctionRegistry().lookup("main", false);
	        if (mainFunction == null) {
	            throw new JSLException("No function main() defined in SL source file.");
	        }
	        return mainFunction;
	     // IO -> for file, Unsupported -> for execute
		} catch (IOException | UnsupportedSpecializationException ex) {
			System.out.print("An error occured during execution of the benchmark: ");
			System.out.println(ex.getMessage());
		}
		return null;
	}
	
	

	// Run the benchmark, write output to file
	private static List<Long> measure(int runNumber, JSLFunction mainFunction, int warmupRuns, int actualRuns, Benchmark benchmark){
		ArrayList<Long> results = new ArrayList<>();
		
		Object[] runValues = new Object[benchmark.getNrArguments()];
		for(int i = 0; i < benchmark.getNrArguments(); i++) {
			runValues[i] = Long.valueOf(benchmark.getRunValues().get(runNumber * benchmark.getNrArguments() + i));
		}
		Object result;
		for(int i = 0; i < warmupRuns; i++) {
			result = mainFunction.getCallTarget().call(runValues);
			
			if (result == null) {
				System.out.println("Something went wrong");
			}
		}
		
		// Run it, time it and store results
		long t1, t2;
		for(int i = 0; i < actualRuns; i++) {
			t1 = System.nanoTime();

			// Run parsed source ON THE GRAAL VM. 
	        result = mainFunction.getCallTarget().call(runValues);

			t2 = System.nanoTime();
			results.add(i, t2-t1);
			if (result == null) {
				System.out.println("Something went wrong, output is null");
				break;
			}
		}
		return results;
	}

	// Cleanup the setup => Write results to CSV file
	private static void finish(int runNumber, int actualRuns, List<Long> results, BufferedWriter out) {
		// Run1 -> 50 values -> total time
		String[] strArr = new String[actualRuns+2];
		long totalTimeNano = 0;
		strArr[0] = "Run " + String.valueOf(runNumber+1);
		for(int i = 0; i < actualRuns; i++) {
			long run = results.get(i);
			totalTimeNano += run;
			strArr[i+1] = String.valueOf(run);
		}
		
		strArr[actualRuns+1] = String.valueOf(totalTimeNano);
		
		System.out.println("Finished measuring input " + (runNumber+1) + "\nTotal time for " + actualRuns + " runs: " + String.valueOf(totalTimeNano/1000000000.0) + " seconds");
		printLine(out, strArr);

	}
	
	private static void printLine(BufferedWriter f, String... args){
		int len = args.length;
		if (len <= 0){
			return;
		}
		try {
			for(int i = 0; i < len-1; i++) {
				f.append(args[i]).append(',');
			}
			f.append(args[len-1]).append('\n');
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	
	private static Benchmark findBenchmark(String name) {
		for(Benchmark benchmark : BenchmarkData.getBenchmarks()) {
			if (benchmark.getName().equals(name)) {
				return benchmark;
			}
		}
		throw new NoSuchElementException();
	}
	
	public static void main(String[] args) {
		String name = args[0];
		int runNr = Integer.parseInt(args[1]);
		Benchmark benchmark = findBenchmark(name);
		int warmupRuns = Integer.parseInt(args[2]);
		int actualRuns = Integer.parseInt(args[3]);
		String resultFilePath = args[4];
		
		PolyglotEngine engine = PolyglotEngine.newBuilder().setIn(System.in).setOut(System.out).build();
		System.out.println("============= Now running on JeSSEL: " + benchmark.getName() + " =============");
		try{		
			BufferedWriter out = new BufferedWriter(new FileWriter(resultFilePath, true));
			JSLFunction runFunc = parseSource(engine, benchmark);
			List<Long> results = measure(runNr, runFunc, warmupRuns, actualRuns, benchmark);
			finish(runNr, actualRuns, results, out);
			out.flush();
		} catch (IOException ex) {
			System.err.println("Could not open file");
		}
		engine.dispose();
	}

}
