package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.dsl.UnsupportedSpecializationException;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.RepeatingNode;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.api.profiles.BranchProfile;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.pl.nodes.PLExpressionNode;
import com.oracle.truffle.pl.nodes.PLStatementNode;

// The loopbody of the WhileNode
// The distinction between the Whileloop and the WhileBody makes it so
// that truffle can optimize it better
public final class PLWhileRepeatingNode extends Node implements RepeatingNode {

	//The loop condition, this is a ExpressionNode because we need a result
	@Child private PLExpressionNode conditionNode;
	
	@Child private PLStatementNode bodyNode;
	
	private final BranchProfile continueTaken = BranchProfile.create();
	private final BranchProfile breakTaken = BranchProfile.create();
	
	// This node does not extend StatementNode. Therefore manually handling the 
	// source information
	private SourceSection sourceSection; 
	
	public PLWhileRepeatingNode(PLExpressionNode conditionNode, PLStatementNode bodyNode) {
		this.conditionNode = conditionNode;
		this.bodyNode = bodyNode;
	}
	
	
	@Override
	public SourceSection getSourceSection() {
		return sourceSection;
	}
	
	public void setSourceSection(SourceSection section) {
		assert this.sourceSection != null : "overwriting Exisiting SourceSection";
		this.sourceSection = section;
	}
	
	@Override
	public boolean executeRepeating(VirtualFrame frame) {
		if (!evaluateCondition(frame)){
			return false;
		}
		
		try {
			bodyNode.executeVoid(frame);
			return true;
		} catch (PLContinueException ex) {
			continueTaken.enter();
			return true;
		} catch (PLBreakException ex) {
			breakTaken.enter();
			return false;
		}
	}
	
	private boolean evaluateCondition(VirtualFrame frame) {
		try {
			return conditionNode.executeBoolean(frame);
		} catch (UnexpectedResultException ex) {
			throw new UnsupportedSpecializationException(this, new Node[]{conditionNode}, ex.getResult());
		}
	}
	
	@Override
	public String toString(){
		return PLStatementNode.formatSourceSection(this);
	}
}