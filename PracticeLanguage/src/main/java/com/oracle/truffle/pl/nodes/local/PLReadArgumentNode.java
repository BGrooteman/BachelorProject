package com.oracle.truffle.pl.nodes.local;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.profiles.BranchProfile;
import com.oracle.truffle.pl.nodes.PLExpressionNode;
import com.oracle.truffle.pl.runtime.PLNull;

// Reads a function argument. Arguments are passed in as object array
// 

public class PLReadArgumentNode extends PLExpressionNode {
	private final int index;
	
	//Profiling information, catches if function was called with
	//less actual arguments than formal arguments.
	private final BranchProfile outOfBoundsTaken = BranchProfile.create();
	
	public PLReadArgumentNode(int index){
		this.index = index;
	}
	
	@Override
	public Object executeGeneric(VirtualFrame frame) {
		Object[] args = frame.getArguments();
		if (index < args.length) {
			return args[index];
		} else {
			//Record that we came in this branch
			outOfBoundsTaken.enter();
			// Return Null
			return PLNull.SINGLETON;
		}
	}
	
}