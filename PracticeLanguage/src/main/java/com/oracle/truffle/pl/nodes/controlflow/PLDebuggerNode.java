package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.debug.DebuggerTags;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLStatementNode;

@NodeInfo(shortName = "debugger", description = "The node implementing a debugger statement")
public final class PLDebuggerNode extends PLStatementNode {
	
	@Override
	public void executeVoid(VirtualFrame frame) {
		// Do nothing
	}
	
	@Override
	protected boolean isTaggedWith(Class<?> tag) {
		if (tag == DebuggerTags.AlwaysHalt.class) {
			return true;
		} else {
			return super.isTaggedWith(tag);
		}
	}
}