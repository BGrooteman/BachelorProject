package com.oracle.truffle.pl.nodes.interop;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.dsl.TypeSystemReference;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.Message;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.interop.UnsupportedMessageException;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.pl.nodes.PLTypes;
import com.oracle.truffle.pl.runtime.PLContext;
import com.oracle.truffle.pl.runtime.PLNull;

@TypeSystemReference(PLTypes.class)
public abstract class PLForeignToPLTypeNode extends Node {
	
	public abstract Object executeConvert(VirtualFrame frame, Object value);
	
	@Specialization
	protected static Object fromObject(Number value) {
		return PLContext.fromForeignValue(value);
	}
	
	@Specialization
	protected static Object fromString(String value) {
		return PLContext.fromForeignValue(value);
	}
	
	@Specialization
	protected static Object fromBoolean(boolean value) {
		return PLContext.fromForeignValue(value);
	}
	
	@Specialization
	protected static Object fromChar(char value) {
		return PLContext.fromForeignValue(value);
	}
	
	//In case the foreign value is boxed, use UNBOX
	@Specialization(guards = "isBoxedPrimitive(frame, value)")
	public Object unbox(VirtualFrame frame, TruffleObject value) {
		Object unboxed = doUnbox(frame, value);
		return PLContext.fromForeignValue(unboxed);
	}
	
	@Specialization(guards = "!isBoxedPrimitive(frame, value)")
	public Object fromTruffleObject(@SuppressWarnings("unused") VirtualFrame frame, TruffleObject value) {
		return value;
	}
	
	@Child private Node isBoxed;
	
	protected final boolean isBoxedPrimitive(VirtualFrame frame, TruffleObject object) {
		if (isBoxed == null) {
			CompilerDirectives.transferToInterpreterAndInvalidate();
			isBoxed = insert(Message.IS_BOXED.createNode());
		}
		return ForeignAccess.sendIsBoxed(isBoxed, frame, object);
	}
	
	@Child private Node unbox;
	
	protected final Object doUnbox(VirtualFrame frame, TruffleObject value) {
		if (unbox == null) {
			CompilerDirectives.transferToInterpreterAndInvalidate();
			unbox = insert(Message.UNBOX.createNode());
		}
		try {
			return ForeignAccess.sendUnbox(unbox, frame, value);
		} catch (UnsupportedMessageException e) {
			return PLNull.SINGLETON;
		}
	}
}
















