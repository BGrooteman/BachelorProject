/**
 * 
 */
package com.oracle.truffle.pl.runtime;

import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.TruffleObject;

// Good practice to redefine the Null value since the native null poses some problems.
// Therefore the Practice null is implemented as a Singleton
public class PLNull implements TruffleObject {
	public static final PLNull SINGLETON = new PLNull();
	
	private PLNull() {
	}
	
	@Override
	public String toString() {
		return "null";
	}

	// Used when some of your objects want to other languages. 
	@Override
	public ForeignAccess getForeignAccess() {
		return PLNullMessageResolutionForeign.createAccess();
	}
}
