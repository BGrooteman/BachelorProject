package com.oracle.truffle.pl.nodes.access;

import com.oracle.truffle.api.CompilerAsserts;
import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Cached;
import com.oracle.truffle.api.dsl.Fallback;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.Message;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.interop.UnknownIdentifierException;
import com.oracle.truffle.api.interop.UnsupportedMessageException;
import com.oracle.truffle.api.interop.UnsupportedTypeException;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.object.DynamicObject;
import com.oracle.truffle.api.object.FinalLocationException;
import com.oracle.truffle.api.object.IncompatibleLocationException;
import com.oracle.truffle.api.object.Location;
import com.oracle.truffle.api.object.Property;
import com.oracle.truffle.api.object.Shape;
import com.oracle.truffle.pl.runtime.PLUndefinedNameException;

@SuppressWarnings("unused")
public abstract class PLWritePropertyCacheNode extends PLPropertyCacheNode {
	
	public abstract void executeWrite(VirtualFrame frame, Object receiver, Object name, Object value);
	
	// Polymorphic inline cache for wrtining properties that already exist
	@Specialization(limit = "CACHE_LIMIT", //
					guards = {
							"cachedName.equals(name)",
							"shapeCheck(shape, receiver)",
							"location != null",
							"canSet(location, value)"
							},
					assumptions = {
							"shape.getValidAssumption()"
					})
	protected static void writeExistingPropertyCached(DynamicObject receiver, Object name, Object value,
					@Cached("name") Object cachedName,
					@Cached("lookupShape(receiver)") Shape shape,
					@Cached("lookupLocation(shape, name, value)") Location location) {
		try {
			location.set(receiver, value, shape);
		} catch (IncompatibleLocationException | FinalLocationException ex) {
			throw new IllegalStateException(ex);
		}
	}
	
	
	// Polymorphic inline cache for writing properties that does not alreadt exist
	@Specialization(limit = "CACHE_LIMIT",
					guards = {
							"namesEqual(cachedName, name)",
							"shapeCheck(oldShape, receiver)",
							"oldLocation == null",
							"canStore(newLocation, value)"
					},
					assumptions = {
							"oldShape.getValidAssumption()",
							"newShape.getValidAssumption()"
							})
	protected static void writeNewPropertyCached(DynamicObject receiver, Object name, Object value,
					@Cached("name") Object cachedName,
					@Cached("lookupShape(receiver)") Shape oldShape,
					@Cached("lookupLocation(oldShape, name, value)") Location oldLocation,
					@Cached("defineProperty(oldShape, name, value)") Shape newShape,
					@Cached("lookupLocation(newShape, name)") Location newLocation) {
		try {
			newLocation.set(receiver, value, oldShape, newShape);
		} catch (IncompatibleLocationException ex) {
			// This will never happen due to the fact that our guard ensures we can store the value.
			throw new IllegalStateException(ex);
		}
	}
	
	//Tries to find given property in the shape
	protected static Location lookupLocation(Shape shape, Object name) {
		CompilerAsserts.neverPartOfCompilation();
		
		Property property = shape.getProperty(name);
		if (property == null) {
			//Could not find property in shape
			return null;
		}
		return property.getLocation();
	}
	
	//Tries to find given property in the shape
	protected static Location lookupLocation(Shape shape, Object name, Object value) {
		Location location = lookupLocation(shape, name);
		
		if (location == null || !location.canSet(value)) {
			//Could not find property in shape
			return null;
		}
		return location;
	}
	
	protected static Shape defineProperty(Shape oldShape, Object name, Object value) {
		return oldShape.defineProperty(name, value, 0);
	}
	
	//These two functions are quite similar:
	//We need this to check if we can call location.set(value)
	protected static boolean canSet(Location location, Object value) {
		return location.canSet(value);
	}
	
	// The more relaxed variant, that does not check if we can Set it, just
	// checks if it would be possible to transition to this location as part of
	// shape change. (See github for more info).
	protected static boolean canStore(Location location, Object value) {
		return location.canStore(value);
	}
	
	@TruffleBoundary
	@Specialization(replaces = {"writeExistingPropertyCached", "writeNewPropertyCached"}, guards = {"isValidPLObject(receiver)"})
	protected static void writeUncached(DynamicObject receiver, Object name, Object value) {
		receiver.define(name, value);
	}
	
	//When no specialization fits
	@Fallback
	protected static void updateShape(Object r, Object name, Object value) {
		// slow-path not in compiled code, but no invalidate need
		CompilerDirectives.transferToInterpreter();
		
		if (!(r instanceof DynamicObject)) {
			throw PLUndefinedNameException.undefinedProperty(name);
		}
		DynamicObject receiver = (DynamicObject) r;
		receiver.updateShape();
		writeUncached(receiver, name, value);
	}
	
	/// LANGUAGE INTEROPERABILITY
	@Specialization(guards = "isForeignObject(receiver)")
	protected static void writeForeign(VirtualFrame frame, TruffleObject receiver, Object name, Object value,
					@Cached("createForeignWriteNode()") Node foreignWriteNode) {
		try {
			ForeignAccess.sendWrite(foreignWriteNode, frame, receiver, name, value);
		} catch (UnknownIdentifierException | UnsupportedTypeException | UnsupportedMessageException e){
			// Foreign access was not succesfull
			throw PLUndefinedNameException.undefinedProperty(name);
		}
	}
	
	protected static Node createForeignWriteNode() {
		return Message.WRITE.createNode();
	}
					
		
}