package com.oracle.truffle.pl.nodes;

import java.io.File;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.instrumentation.Instrumentable;
import com.oracle.truffle.api.instrumentation.StandardTags;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.SourceSection;


@NodeInfo(language = "PL", description = "The abstract base node for all PL statements")
@Instrumentable(factory = PLStatementNodeWrapper.class)
public abstract class PLStatementNode extends Node {
	
	private SourceSection sourceSection;
	private boolean hasStatementTag;
	private boolean hasRootTag;
	
	@Override
	public final SourceSection getSourceSection() {
		return sourceSection;
	}
	
	public void setSourceSection(SourceSection sourceSection) {
		assert this.sourceSection == null : "overwriting existing SourceSection";
		this.sourceSection = sourceSection;
	}
	
	// Execute as statement, no returnvalue
	public abstract void executeVoid(VirtualFrame frame);
	
	//Tags this node as StatementTag for instrumention purposes
	public final void addStatementTag() {
		hasStatementTag = true;
	}
	
	public final void addRootTag() {
		hasRootTag = true;
	}
	
	@Override
	protected boolean isTaggedWith(Class<?> tag) {
		if (tag == StandardTags.StatementTag.class) {
			return hasStatementTag;
		} else if (tag == StandardTags.RootTag.class) {
			return hasRootTag;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return formatSourceSection(this);
	}
	
	// Formats a source section into human readable form. Sometimes needs to follow hierarchy
	// to find source, in that case a ~ is added at the end.
	public static String formatSourceSection(Node node) {
		if (node == null) {
			return "<unknown>";
		}
		SourceSection section = node.getSourceSection();
		boolean estimated = false;
		if (section == null) {
			section = node.getEncapsulatingSourceSection();
			estimated = true;
		}
		
		if (section == null || section.getSource() == null) {
			return "<unknown source>";
		} else {
			String sourceName = new File(section.getSource().getName()).getName();
			int startLine = section.getStartLine();
			return String.format("%s:%d%s", sourceName, startLine, estimated ? "~" : "");
		}
		
	}
	
	
}
