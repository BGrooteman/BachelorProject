\chapter{Implementation}
To implement a language in Truffle the nodes that will make up the Abstract Syntax Tree (AST) have to be specified. An AST built of these nodes then needs to be constructed from inputted source code. This AST then can be passed to the Polyglot engine, this will use the nodes to execute the code. This Polyglot engine is totally abstracted away and provided by Truffle, and handles all the low level implementation. 

\section{The Pipeline}
Before source code is ran in JeSSEL it goes through a couple of steps. In the figure below can this pipeline be seen.\\
\begin{figure}[ht!]
\centering
\includegraphics[width = \textwidth]{Figures/Pipeline.png}
\caption{Pipeline}
\end{figure}
\newpage
When a JeSSEL program is executed, the source code is loaded into JeSSEL by the main class (\texttt{JSLMain}). \texttt{JSLMain} also creates the PolyglotEngine and registers the \texttt{JSLLanguage} in it. JSLLanguage is a singleton class that is used to execute the source as a JeSSEL source. This is designed in such a way that multiple languages could be registered to the PolyglotEngine and could all be ran from the same command (by loading a source into the JSLMain class). The PolyglotEngine decides based on the MIMEType of the source which language needs to be executed and invokes the \texttt{executeSource} function of that language. This function needs to create a \texttt{CallTarget} and return that to the Engine. A CallTarget is the root of the AST from which execution should begin. In \texttt{JSLLanguage} this is done by redirecting the Source to the JSLParser. Here the source gets parsed by the JavaParser and turned into a AST (JPAST). This AST is buildup from JavaParser Nodes while we need them to be buildup from JeSSEL nodes. The translation is done by the \texttt{TreeTranslatorVisitor} which uses the Visitor pattern to walk over the JSAST. \\
Since we want to generate expressions as well as statements, therefore keeping track of this in the visitor is not desirable. The generation of the new JeSSEL AST is thus done by the \texttt{NodeFactory}, who has two stacks to keep track of the statements and expressions.
When the visitor has visited the whole tree, we have a Map that contains all functions in the source, each with their own AST's. The \texttt{JSLLanguage} uses this map to extract the main function from this map, and uses it to build a CallTarget, which is then passed to the Engine and executed.

\section{Types}
Truffle is a framework built for building interpreters, mostly focused on dynamically typed languages. Since we want to use it for a statically typed (Java-like) language we need to work around this.\\
To keep track of the types of expressions, we introduced an extra \texttt{class} which all the expressionNodes implement. This \texttt{class}, called \texttt{JSLTypedExpressionNode}, imposes the constraint that all children should implement a \texttt{getType()} function. This means that we know each Expression in our language has a type, which can then be used do typechecks during the translation process. By doing so we lose some of the advertised property of Truffle (the self-specializing AST that uses type-feedback to do so) but make sure the language is typed (as Java would be). 
Another solution would be to only do type checking during parsing and after that letting Truffle do the optimizing. A downside to this is that it would result in having to write guards that show which nodes should be specialized, which would take a lot of time, and clutter up the code. This approach was discarded due to the fact that every node would need code to differentiate between the different types at runtime, which would lead to a lot of duplicate code. 

\section{JeSSEL in Truffle}
JeSSEL makes use of Truffle in multiple ways, here I will show some examples and explain why they are useful.
\subsection{Annotations}
Truffle has multiple annotations that are used in JeSSEL. These are pointers for the underlaying framework that something needs to be done with the function or class.
Some of the most used are explained here: \\
\textbf{@Specialization:} This is an annotation that can be put above a function. It tells Truffle that the function is a specialization of an operation. These specializations can  have guards which will then be used at runtime to determine which of the specializations should be used. In Dynamic languages this is quite a useful feature since it makes it so you can define multiple functions for the same operation with different inputs. In JeSSEL we have types which make this annotation less useful, and while it is possible to still write guards that distinguish between different types of input, this will get messy quick. 
On a mailing list I found a conversation about this problem, and the people from Truffle advised to split the operations in different files and just differentiate between them during parsing. Link to that mailing list: \href{http://mail.openjdk.java.net/pipermail/graal-dev/2016-July/004472.html}{http://mail.openjdk.java.net/pipermail/graal-dev/2016-July/004472.html} So that is how it is implemented in JeSSEL.\\
\textbf{@ExplodeLoop:} This is an annotation that can be matched with a function that contains a loop with a fixed length. When this is the case the \texttt{ExplodeLoop} can be used to tell the compiler to roll the loop out at compilation. One of the examples is in the BlockNodes, which consists of a list of bodyNodes that needs to be ran. We can tell the compiler to get rid of the loop and unroll the elements.\\
\begin{lstlisting}[caption=ExplodeLoop annotation in JSLBlockNode, tabsize = 2, numbers=left]
	@ExplodeLoop
	@Override
	public void executeVoid(VirtualFrame frame) {
		for(JSLStatementNode statement : bodyNodes) {
			statement.executeVoid(frame);
		}
	}
\end{lstlisting}
\textbf{@TruffleBoundary:} This is an annotation that can be matched with a function. It tells Truffle that the function should never be compiled. This can be put at functions that are a lot of code but are not used very often. You do not want these functions cluttering the compiled code (which would make is slow), we always want it to be handled by the interpreter at runtime.\\

\textbf{Usage of the Annotations in JeSSEL}\\
In Table 3.1 can every usage of these Annotations be found. This gives an overview of where the Truffle compiler will (try to) optimize the code.\\
\begin{table}[ht!]
\centering\setlength\extrarowheight{2pt}
\caption{Input Values of each benchmark}
\begin{tabular}{@{}l p{0.6\linewidth}}
\toprule
\textbf{Annotation} & \textbf{Where}\\
\multirow{3}{*}{@Specialization}
& 5x in JSLPrintlnBuiltin\\
& 1x in JSLTimeBuiltin\\
& 3x in DispatchNode\\
\addlinespace

\multirow{2}{*}{@ExplodeLoop} 	
		& 1x in InvokeNode \\
		& 1x in BlockNode \\
\addlinespace

\multirow{2}{*}{@TruffleBoundary} 
		& 5x in JSLPrintlnBuiltin \\
 		& 1x in JSLTimeBuiltin\\
\bottomrule
\end{tabular}
\end{table}
In addition to the @Specialization annotations mentioned in Table 3.1, this annotation is also used in \textit{every} ExpressionNode in the \texttt{expression} package (34x) and in \textit{every} read and write node in the \texttt{local} package (25x). These, however, do not really add any functionality since we differentiate between those nodes during parsing but are just there because Truffle requires them to be there. 

\subsection{Profiling}
Truffle provides Profiles which are basically counters. These are especially useful in controlflow statements. The idea is that Truffle keeps track of how often a branch is taken, and optimizes itself based on the assumption that if we took the if-branch nine times in a row, the tenth time will have a large chance to be the if-branch again. Behind the scenes Truffle rearranges it's code to be able to execute the expected branch faster.
\begin{lstlisting}[caption=Minimalized example of a profile, tabsize = 2, numbers=left]
public final class JSLIfNode extends JSLStatementNode {
	@Child private JSLExpressionNode conditionNode;
	private final ConditionProfile condition = 
		ConditionProfile.createCountingProfile();
	
	@Override
	public void executeVoid(VirtualFrame frame) {
		if(condition.profile(evaluateCondition(frame))) {
			doIf();
		} else {
			doElse();
		}
}
\end{lstlisting}
This is also a nice example of a Truffle feature that directly helps to optimize the interpreter, would probably not be the first thing that is implemented when a interpreter is written from scratch, and is very easy to use.

\subsection{Implementation of JeSSEL}
By using Truffle it should be easier to write an interpreter. For JeSSEL, the final implementation totaled around 5100 lines of code. Table 3.2 show how these SLOC is distributed over the project.
\begin{table}[ht]
\centering\setlength\extrarowheight{2pt}
\caption{SLOC per package}

\begin{tabular}{@{}lr p{0.6\linewidth}}
\toprule
Package name		& SLOC 	& Package Description \\
\midrule
main 					& 250	 	& Top level package, holds the main classes \\
runtime 				& 425 		& Holds the runtime Exceptions and the JSLContext, which is used to lookup values at runtime\\
parser					& 1541 	& Translates the JavaParser AST into a usable JeSSEL AST\\ 
builtin				& 98 		& Holds the implementation of the builtin nodes\\
node					  & 554 		& The top level package for all AST nodes, holds the abstract JSLStatement and JSLTypedExpression classes that all the nodes use. \\
expression	 		& 863 		& This package holds all the nodes that represent expressions. Nodes for all binary expressions\\
local				 	& 881 		& Holds all the nodes that are used to create, write and read variables in the local scope.\\
controlflow	 	& 337 		& All the controlflow statement nodes can be found in this package, the \\
interop				& 74 		& Code for interoperability between languages that run on the GraalVM.  \\
call						& 140 		& Holds the invoke and dispatch nodes that handle the function calls\\
\addlinespace
\textbf{Total} & 5163 & \\
\bottomrule
\end{tabular}
\end{table}
The parser package is the most SLOC, which is due to the fact that we transform the JavaParser way in a peculiar way. If a proper parser would be written, or the visitor pattern would be used to translate the JavaParser AST to the proper AST, this could be reduced a lot.\\
The full source code of JeSSEL can be found on GitLab \\\href{https://gitlab.com/BGrooteman/BachelorProject/tree/master/JeSSEL}{https://gitlab.com/BGrooteman/BachelorProject/tree/master/JeSSEL}.\\