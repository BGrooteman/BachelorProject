package com.BScProject.truffle.jsl.runtime;

import com.BScProject.truffle.jsl.JSLLanguage;
import com.oracle.truffle.api.interop.CanResolve;
import com.oracle.truffle.api.interop.MessageResolution;
import com.oracle.truffle.api.interop.Resolve;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.nodes.Node;

@MessageResolution(receiverType = JSLNull.class, language = JSLLanguage.class)
public class JSLNullMessageResolution {
	
	@Resolve(message = "IS_NULL")
	public abstract static class JSLForeignIsNullNode extends Node {
		public Object access(Object receiver) {
			return JSLNull.SINGLETON == receiver;
		}
	}
	
	@CanResolve
	public abstract static class CheckNull extends Node {
		public static boolean test(TruffleObject receiver) {
			return receiver instanceof JSLNull;
		}
	}
}