package com.oracle.truffle.pl.builtins;

import java.io.BufferedReader;
import java.io.IOException;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.PLException;

@NodeInfo(shortName = "readln")
public abstract class PLReadlnBuiltin extends PLBuiltinNode {
	@Specialization
	public String readln() {
		String result = doRead(getContext().getInput());
		if (result == null) {
			// Just return the empty string
			return "";
		}
		return result;
	}
	
	@TruffleBoundary
	private static String doRead(BufferedReader in) {
		try {
			return in.readLine();
		} catch (IOException ex) {
			throw new PLException(ex.getMessage());
		}
	}
}