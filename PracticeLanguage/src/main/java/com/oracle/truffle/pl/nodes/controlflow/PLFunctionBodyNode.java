package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.profiles.BranchProfile;
import com.oracle.truffle.pl.nodes.PLExpressionNode;
import com.oracle.truffle.pl.nodes.PLStatementNode;
import com.oracle.truffle.pl.runtime.PLNull;

@NodeInfo(shortName = "body")
public final class PLFunctionBodyNode extends PLExpressionNode {
	
	// Body of the function
	@Child private PLStatementNode bodyNode;
	
	//Profiling information, kept track of by compiler
	private final BranchProfile exceptionTaken = BranchProfile.create();
	private final BranchProfile nullTaken = BranchProfile.create();
	
	public PLFunctionBodyNode(PLStatementNode bodyNode) {
		this.bodyNode = bodyNode;
		addRootTag();
	}
	
	public Object executeGeneric(VirtualFrame frame) {
		try {
			bodyNode.executeVoid(frame);
		} catch (PLReturnException ex) {
			// Register function has explicit return type
			exceptionTaken.enter();
			return ex.getResult();
		}
		//Register function has no return
		nullTaken.enter();
		return PLNull.SINGLETON;
	}
	
}