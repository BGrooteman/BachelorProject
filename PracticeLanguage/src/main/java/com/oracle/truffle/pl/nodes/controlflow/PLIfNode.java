package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.dsl.UnsupportedSpecializationException;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.api.profiles.ConditionProfile;
import com.oracle.truffle.pl.nodes.PLExpressionNode;
import com.oracle.truffle.pl.nodes.PLStatementNode;

@NodeInfo(shortName = "if", description = "The node implementing a conditional statement")
public final class PLIfNode extends PLStatementNode {
	
	//Use ExpressionNode for condition because we need return value
	@Child private PLExpressionNode conditionNode;
	
	@Child private PLStatementNode thenPartNode;
	
	@Child private PLStatementNode elsePartNode;
	
	//Capturing profiling information
	// Use CountingProfile over BinaryProfile to transmit the probabilty of condition to be true
	private final ConditionProfile condition = ConditionProfile.createCountingProfile();
	
	public PLIfNode(PLExpressionNode conditionNode, PLStatementNode thenPartNode, PLStatementNode elsePartNode) {
		this.conditionNode = conditionNode;
		this.thenPartNode = thenPartNode;
		this.elsePartNode = elsePartNode;
	}
	
	@Override
	public void executeVoid(VirtualFrame frame) {
		if(condition.profile(evaluateCondition(frame))){
			thenPartNode.executeVoid(frame);
		} else {
			if(elsePartNode != null) {
				elsePartNode.executeVoid(frame);
			}
		}
	}
	
	
	private boolean evaluateCondition(VirtualFrame frame){
		try {
			return conditionNode.executeBoolean(frame);
		} catch (UnexpectedResultException ex) {
			throw new UnsupportedSpecializationException(this, new Node[]{conditionNode}, ex.getResult());
		}
	}
}