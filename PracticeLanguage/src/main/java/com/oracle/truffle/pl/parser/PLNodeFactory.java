package com.oracle.truffle.pl.parser;

import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.pl.nodes.PLExpressionNode;
import com.oracle.truffle.pl.nodes.PLRootNode;
import com.oracle.truffle.pl.nodes.PLStatementNode;
import com.oracle.truffle.pl.nodes.access.PLReadPropertyNode;
import com.oracle.truffle.pl.nodes.access.PLReadPropertyNodeGen;
import com.oracle.truffle.pl.nodes.access.PLWritePropertyNode;
import com.oracle.truffle.pl.nodes.access.PLWritePropertyNodeGen;
import com.oracle.truffle.pl.nodes.call.PLInvokeNode;
import com.oracle.truffle.pl.nodes.controlflow.PLBlockNode;
import com.oracle.truffle.pl.nodes.controlflow.PLBreakNode;
import com.oracle.truffle.pl.nodes.controlflow.PLContinueNode;
import com.oracle.truffle.pl.nodes.controlflow.PLDebuggerNode;
import com.oracle.truffle.pl.nodes.controlflow.PLFunctionBodyNode;
import com.oracle.truffle.pl.nodes.controlflow.PLIfNode;
import com.oracle.truffle.pl.nodes.controlflow.PLReturnNode;
import com.oracle.truffle.pl.nodes.controlflow.PLWhileNode;
import com.oracle.truffle.pl.nodes.expression.PLAddNodeGen;
import com.oracle.truffle.pl.nodes.expression.PLBigIntegerLiteralNode;
import com.oracle.truffle.pl.nodes.expression.PLDivNodeGen;
import com.oracle.truffle.pl.nodes.expression.PLEqualNodeGen;
import com.oracle.truffle.pl.nodes.expression.PLFunctionLiteralNode;
import com.oracle.truffle.pl.nodes.expression.PLLessOrEqualNodeGen;
import com.oracle.truffle.pl.nodes.expression.PLLessThanNodeGen;
import com.oracle.truffle.pl.nodes.expression.PLLogicalAndNode;
import com.oracle.truffle.pl.nodes.expression.PLLogicalNotNodeGen;
import com.oracle.truffle.pl.nodes.expression.PLLogicalOrNode;
import com.oracle.truffle.pl.nodes.expression.PLLongLiteralNode;
import com.oracle.truffle.pl.nodes.expression.PLMulNodeGen;
import com.oracle.truffle.pl.nodes.expression.PLParenExpressionNode;
import com.oracle.truffle.pl.nodes.expression.PLStringLiteralNode;
import com.oracle.truffle.pl.nodes.expression.PLSubNodeGen;
import com.oracle.truffle.pl.nodes.local.PLReadArgumentNode;
import com.oracle.truffle.pl.nodes.local.PLReadLocalVariableNode;
import com.oracle.truffle.pl.nodes.local.PLReadLocalVariableNodeGen;
import com.oracle.truffle.pl.nodes.local.PLWriteLocalVariableNode;
import com.oracle.truffle.pl.nodes.local.PLWriteLocalVariableNodeGen;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Helperclass used by parser to create nodes
public class PLNodeFactory {
	
	static class LexicalScope {
		protected final LexicalScope outer;
		protected final Map<String, FrameSlot> locals;
		
		LexicalScope(LexicalScope outer) {
			this.outer = outer;
			this.locals = new HashMap<>();
			if (outer != null) {
				locals.putAll(outer.locals);
			}
		}
	}
	
	
	
	// While parsing a source unit
	private final Source source;
	private final Map<String, PLRootNode> allFunctions;
	
	//While parsing a function;
	private int functionStartPos;
	private String functionName;
	private int functionBodyStartPos;
	private int parameterCount;
	private FrameDescriptor frameDescriptor;
	private List<PLStatementNode> methodNodes;
	
	//While parsing a block
	private LexicalScope lexicalScope;
	
	public PLNodeFactory(Source source) {
		this.source = source;
		this.allFunctions = new HashMap<>();
	}
	
	public Map<String, PLRootNode> getAllFunctions() {
		return allFunctions;
	}
	
	public void startFunction(Token nameToken, int bodyStartPos) {
		assert functionStartPos == 0;
		assert functionName == null;
		assert functionBodyStartPos == 0;
		assert parameterCount == 0;
		assert frameDescriptor == null;
		assert lexicalScope == null;
		
		functionStartPos = nameToken.charPos;
		functionName = nameToken.val;
		functionBodyStartPos = bodyStartPos;
		frameDescriptor = new FrameDescriptor();
		methodNodes = new ArrayList<>();
		startBlock();
	}
	
	
	
	public void addFormalParameter(Token nameToken) {
		
		
		final PLReadArgumentNode readArg = new PLReadArgumentNode(parameterCount);
		PLExpressionNode assignment = createAssignment(createStringLiteral(nameToken, false), readArg);
		methodNodes.add(assignment);
		parameterCount++;
	}
	
	public void finishFunction(PLStatementNode bodyNode) {
		methodNodes.add(bodyNode);
		final int bodyEndPos = bodyNode.getSourceSection().getCharEndIndex();
		final SourceSection functionSrc = source.createSection(functionStartPos, bodyEndPos - functionStartPos);
		final PLStatementNode methodBlock = finishBlock(methodNodes, functionBodyStartPos, bodyEndPos - functionStartPos);
		assert lexicalScope == null : "Wrong scoping of blocks in parser";
		
		final PLFunctionBodyNode functionBodyNode = new PLFunctionBodyNode(methodBlock);
		functionBodyNode.setSourceSection(functionSrc);
		final PLRootNode rootNode = new PLRootNode(frameDescriptor, functionBodyNode, functionSrc, functionName);
		allFunctions.put(functionName, rootNode);
		
		functionStartPos = 0;
		functionName = null;
		functionBodyStartPos = 0;
		parameterCount = 0;
		frameDescriptor = null;
		lexicalScope = null;	
	}
	
	public void startBlock() {
		lexicalScope = new LexicalScope(lexicalScope);
	}
	
	public PLStatementNode finishBlock(List<PLStatementNode> bodyNodes, int startPos, int length) {
		lexicalScope = lexicalScope.outer;
		List<PLStatementNode> flattenedNodes = new ArrayList<>(bodyNodes.size());
		flattenBlocks(bodyNodes, flattenedNodes);
		for (PLStatementNode statement : flattenedNodes) {
			SourceSection sourceSection = statement.getSourceSection();
			if (sourceSection != null && !isHaltInCondition(statement)) {
				statement.addStatementTag();
			}
		}
		PLBlockNode blockNode = new PLBlockNode(flattenedNodes.toArray(new PLStatementNode[flattenedNodes.size()]));
		blockNode.setSourceSection(source.createSection(startPos, length));
		return blockNode;
	}
	
	private static boolean isHaltInCondition(PLStatementNode statement) {
		return (statement instanceof PLIfNode) || (statement instanceof PLWhileNode);
	}
	
	private void flattenBlocks(Iterable<? extends PLStatementNode> bodyNodes, List<PLStatementNode> flattenedNodes) {
		for (PLStatementNode n : bodyNodes) {
			if (n instanceof PLBlockNode) {
				flattenBlocks(((PLBlockNode) n).getStatements(), flattenedNodes);
			} else {
				flattenedNodes.add(n);
			}
		}
	}
	
	//returns link to debugger
	PLStatementNode createDebugger(Token debuggerToken) {
		final PLDebuggerNode debuggerNode = new PLDebuggerNode();
		srcFromToken(debuggerNode, debuggerToken);
		return debuggerNode;
	}
	
	//returns Node of breakToken
	public PLStatementNode createBreak(Token breakToken) {
		final PLBreakNode breakNode = new PLBreakNode();
		srcFromToken(breakNode, breakToken);
		return breakNode;
	}
	
	//returns Node of continue
	public PLStatementNode createContinue(Token continueToken) {
		final PLContinueNode continueNode = new PLContinueNode();
		srcFromToken(continueNode, continueToken);
		return continueNode;
	}
	
	public PLStatementNode createWhile(Token whileToken, PLExpressionNode conditionNode, PLStatementNode bodyNode) {
		conditionNode.addStatementTag();
		final int start = whileToken.charPos;
		final int end = bodyNode.getSourceSection().getCharEndIndex();
		final PLWhileNode whileNode = new PLWhileNode(conditionNode, bodyNode);
		whileNode.setSourceSection(source.createSection(start, end-start));
		return whileNode;
	}
	
	
	public PLStatementNode createIf(Token ifToken, PLExpressionNode conditionNode, PLStatementNode thenPartNode, PLStatementNode elsePartNode) {
		conditionNode.addStatementTag();
		final int start = ifToken.charPos;
		final int end = elsePartNode == null ? thenPartNode.getSourceSection().getCharEndIndex() : elsePartNode.getSourceSection().getCharEndIndex();
		final PLIfNode ifNode = new PLIfNode(conditionNode, thenPartNode, elsePartNode);
		ifNode.setSourceSection(source.createSection(start, end-start));
		return ifNode;
	}
	
	
	public PLStatementNode createReturn(Token t, PLExpressionNode valueNode) {
		final int start = t.charPos;
		final int length = valueNode == null ? t.val.length() : valueNode.getSourceSection().getCharEndIndex() - start;
		final PLReturnNode returnNode = new PLReturnNode(valueNode);
		returnNode.setSourceSection(source.createSection(start, length));
		return returnNode;
	}
	
	public PLExpressionNode createBinary(Token opToken, PLExpressionNode leftNode, PLExpressionNode rightNode) {
		final PLExpressionNode result;
		switch (opToken.val) {
			case "+":
				result = PLAddNodeGen.create(leftNode, rightNode);
				break;
			case "*":
				result = PLMulNodeGen.create(leftNode, rightNode);
				break;
			case "/":
				result = PLDivNodeGen.create(leftNode, rightNode);
				break;
			case "-":
				result = PLSubNodeGen.create(leftNode, rightNode);
				break;
			case "<":
				result = PLLessThanNodeGen.create(leftNode, rightNode);
				break;
			case "<=":
				result = PLLessOrEqualNodeGen.create(leftNode, rightNode);
				break;
			case ">":
				result = PLLogicalNotNodeGen.create(PLLessOrEqualNodeGen.create(leftNode, rightNode));
				break;
			case ">=":
				result = PLLogicalNotNodeGen.create(PLLessThanNodeGen.create(leftNode, rightNode));
				break;
			case "==":
				result = PLEqualNodeGen.create(leftNode, rightNode);
				break;
			case "!=":
				result = PLLogicalNotNodeGen.create(PLEqualNodeGen.create(leftNode, rightNode));
				break;
			case "&&":
				result = new PLLogicalAndNode(leftNode, rightNode);
				break;
			case "||":
				result = new PLLogicalOrNode(leftNode, rightNode);
			default:
				throw new RuntimeException("unexpected operation: " + opToken.val);
		}
		int start = leftNode.getSourceSection().getCharIndex();
		int length = rightNode.getSourceSection().getCharEndIndex() - start;
		result.setSourceSection(source.createSection(start, length));
		
		return result;
	}
	
	public PLExpressionNode createCall(PLExpressionNode functionNode, List<PLExpressionNode> parameterNodes, Token finalToken){
		final PLExpressionNode result = new PLInvokeNode(functionNode, parameterNodes.toArray(new PLExpressionNode[parameterNodes.size()]));
		
		final int startPos = functionNode.getSourceSection().getCharIndex();
		final int endPos = finalToken.charPos + finalToken.val.length();
		result.setSourceSection(source.createSection(startPos, endPos - startPos));
		
		return result;
	}
	
	public PLExpressionNode createAssignment(PLExpressionNode nameNode, PLExpressionNode valueNode) {
		String name = ((PLStringLiteralNode) nameNode).executeGeneric(null);
		FrameSlot frameSlot = frameDescriptor.findOrAddFrameSlot(name);
		lexicalScope.locals.put(name, frameSlot);
		final PLExpressionNode result = PLWriteLocalVariableNodeGen.create(valueNode, frameSlot);
		
		if(valueNode.getSourceSection() != null) {
			final int start = nameNode.getSourceSection().getCharIndex();
			final int length = valueNode.getSourceSection().getCharEndIndex() - start;
			result.setSourceSection(source.createSection(start, length));
		}
		
		return result;
	}
	
	
	public PLExpressionNode createRead(PLExpressionNode nameNode) {
		String name = ((PLStringLiteralNode) nameNode).executeGeneric(null);
		final PLExpressionNode result;
		final FrameSlot frameSlot = lexicalScope.locals.get(name);
		if(frameSlot != null) {
			result = PLReadLocalVariableNodeGen.create(frameSlot);
		} else {
			result = new PLFunctionLiteralNode(name);
		}
		result.setSourceSection(nameNode.getSourceSection());
		return result;
	}
	
	public PLExpressionNode createStringLiteral(Token literalToken, boolean removeQuotes) {
		String literal = literalToken.val;
		if(removeQuotes) {
			assert literal.length() >= 2 && literal.startsWith("\"") && literal.endsWith("\"");
			literal = literal.substring(1, literal.length()-1);
		}
		final PLStringLiteralNode result = new PLStringLiteralNode(literal.intern());
		srcFromToken(result, literalToken);
		return result;
	}
	
	public PLExpressionNode createNumericLiteral(Token literalToken) {
		PLExpressionNode result;
		try {
			result = new PLLongLiteralNode(Long.parseLong(literalToken.val));
		} catch (NumberFormatException ex) {
			result = new PLBigIntegerLiteralNode(new BigInteger(literalToken.val));
		}
		srcFromToken(result, literalToken);
		return result;
	}
	
	public PLExpressionNode createParenExpression(PLExpressionNode expressionNode, int start, int length) {
		final PLParenExpressionNode result = new PLParenExpressionNode(expressionNode);
		result.setSourceSection(source.createSection(start, length));
		return result;
	}
	
	public PLExpressionNode createReadProperty(PLExpressionNode receiverNode, PLExpressionNode nameNode) {
		final PLExpressionNode result = PLReadPropertyNodeGen.create(receiverNode, nameNode);
		
		final int startPos = receiverNode.getSourceSection().getCharIndex();
		final int endPos = nameNode.getSourceSection().getCharEndIndex();
		result.setSourceSection(source.createSection(startPos, endPos - startPos));
		return result;
	}
	
	public PLExpressionNode createWriteProperty(PLExpressionNode receiverNode, PLExpressionNode nameNode, PLExpressionNode valueNode) {
		final PLExpressionNode result = PLWritePropertyNodeGen.create(receiverNode, nameNode, valueNode);
		
		final int startPos = receiverNode.getSourceSection().getCharIndex();
		final int endPos = nameNode.getSourceSection().getCharEndIndex();
		result.setSourceSection(source.createSection(startPos, endPos - startPos));
		
		return result;
	}
	
	private void srcFromToken(PLStatementNode node, Token token) {
		node.setSourceSection(source.createSection(token.charPos, token.val.length()));
	}
}