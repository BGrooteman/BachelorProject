package com.oracle.truffle.pl.nodes.expression;

import java.math.BigInteger;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLBinaryNode;

@NodeInfo(shortName = "<=")
public abstract class PLLessOrEqualNode extends PLBinaryNode {
	
	@Specialization
	protected boolean lessOrEqual(long left, long right) {
		return left <= right;
	}
	
	@Specialization
	@TruffleBoundary
	protected boolean lessOrEqual(BigInteger left, BigInteger right) {
		return left.compareTo(right) <= 0;
	}
}