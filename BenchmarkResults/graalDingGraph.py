import csv
import numpy as np
import matplotlib.pyplot as plt

def truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])

benchmark_names = ["While", "PrimeFinder", "GCD", "Fibonacci", "Factorial", "FactorCounter"]
total_times_jes = []
total_times_recaf  = []
total_times_recaf_graal = []
for name in benchmark_names:
	path_jessel = "./Jessel/" + name + ".csv"
	path_recaf = "./Recaf/" + name + ".csv"
	path_recaf_graal = "./Graal/" + name + "OnGraal.csv"
	with open(path_jessel, 'r') as f_j, open (path_recaf, 'r') as f_r, open(path_recaf_graal, 'r') as f_rg:
		jessel_data = list(csv.reader(f_j))
		recaf_data	= list(csv.reader(f_r))
		recaf_graal_data = list(csv.reader(f_rg))

	#One bar graph where we see the relative speedup of JeSSEL
	list_jes = []
	list_recaf = []
	list_recaf_graal = []

	for i in range(4):
		list_jes.append(int(jessel_data[i+1][-1]))
		list_recaf.append(int(recaf_data[i+1][-1]))
		list_recaf_graal.append(int(recaf_graal_data[i+1][-1]))

	total_times_jes.append(list_jes)
	total_times_recaf.append(list_recaf)
	total_times_recaf_graal.append(list_recaf_graal)
speedup_jes = []
speedup_graal = []

for (l1, l2) in list(zip(total_times_jes, total_times_recaf)):
	# print(l1 + l2)
	speedup_jes.append([(recaf/jessel) for jessel, recaf in zip(l1, l2)])

for (l1, l2) in list(zip(total_times_recaf_graal, total_times_recaf)):
	# print(l1 + l2)
	speedup_graal.append([(recaf/graal) for graal, recaf in zip(l1, l2)])
	

with open("./Graal/relativeSpeedupWithGraal.csv", "w") as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(["", "Input 1", "Input 2", "Input 3", "Input 4"])
        for name, speedup_j, speedup_g in zip(benchmark_names, speedup_jes, speedup_graal):
            strList_j = [str(truncate(x, 4)) for x in speedup_j]
            strList_g = [str(truncate(x, 4)) for x in speedup_g]
           # writer.writerow([name] + ["JeSSEL"] + strList_j)
            writer.writerow([name] + ["RecafOnGraal"] + strList_g)

for benchmark, jtime, rtime, gtime in zip(benchmark_names, total_times_jes, total_times_recaf, total_times_recaf_graal):
	with open("./Graal/runtimes"+ benchmark + ".csv", "w") as csv_file:
		writer = csv.writer(csv_file, delimiter=',')
		writer.writerow(["", "Input 1", "Input 2", "Input 3", "Input 4"])
		writer.writerow(["JeSSEL"] + list(map(lambda x: truncate(x/1000000000, 4), jtime)))
		writer.writerow(["Recaf"] + list(map(lambda x: truncate(x/1000000000,4), rtime)))
		writer.writerow(["RecafOnGraal"] + list(map(lambda x: truncate(x/1000000000,4), gtime)))
