package com.oracle.truffle.pl;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.spi.FileTypeDetector;

public final class PLFileDetector extends FileTypeDetector {
	@Override
	public String probeContentType(Path path) throws IOException {
		if (path.getFileName().toString().endsWith(".pl")) {
			return PLLanguage.MIME_TYPE;
		}
		return null;
	}
}