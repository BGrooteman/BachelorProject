package com.oracle.truffle.pl.nodes.expression;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.CompilerDirectives.CompilationFinal;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.PLLanguage;
import com.oracle.truffle.pl.nodes.PLExpressionNode;
import com.oracle.truffle.pl.runtime.PLContext;
import com.oracle.truffle.pl.runtime.PLFunction;

@NodeInfo(shortName = "func")
public final class PLFunctionLiteralNode extends PLExpressionNode {
	private final String functionName;
	
	@CompilationFinal private PLFunction cachedFunction;
	
	public PLFunctionLiteralNode(String functionName) {
		this.functionName = functionName;
	}
	
	@Override
	public PLFunction executeGeneric(VirtualFrame frame) {
		if(cachedFunction == null){
			// Change the @CompilationFinal field, since we now know the Context
			CompilerDirectives.transferToInterpreterAndInvalidate();
			//First execution of the loop, lookup context and register
			PLContext context = PLLanguage.INSTANCE.findContext();
			cachedFunction = context.getFunctionRegistry().lookup(functionName, true);
		}
		return cachedFunction;
	}
	
	
}