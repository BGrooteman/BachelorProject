package com.oracle.truffle.pl.nodes.local;

import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.NodeField;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.frame.FrameSlotKind;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

// This node can write local variables to the VirtualFrame
// Truffle allows to store primitives and Object values.

@NodeChild("valueNode")
@NodeField(name = "slot", type = FrameSlot.class)
public abstract class PLWriteLocalVariableNode extends PLExpressionNode {
	
	// Returns the descriptor of the accessed local variable
	protected abstract FrameSlot getSlot();
	
	
	//Specialized write to Long.
	// Either already long type of empty
	@Specialization(guards = "isLongOrIllegal(frame)")
	protected long writeLong(VirtualFrame frame, long value) {
		// Set type, if already set doesnt matter.
		getSlot().setKind(FrameSlotKind.Long);
		frame.setLong(getSlot(), value);
		return value;
	}
	
	@Specialization(guards = "isBooleanOrIllegal(frame)")
	protected boolean writeBoolean(VirtualFrame frame, boolean value) {
		getSlot().setKind(FrameSlotKind.Boolean);
		frame.setBoolean(getSlot(), value);
		return value;
	}
	
	// Generic write that works for all types.
	// Use this over @FallBack since once we are here we know we will
	// never respecialize (FallBack will try to do the other specializations 
	// before doing this one. This Specialization replaces them == more speed).
	@Specialization(replaces = {"writeLong", "writeBoolean"})
	protected Object write(VirtualFrame frame, Object value){
		// Change type to Object
		getSlot().setKind(FrameSlotKind.Object);
		
		frame.setObject(getSlot(), value);
		return value;
	}
	
	//Guard functions.
	//Param explaination same as in PLReadLocalVariableNode.
	protected boolean isLongOrIllegal(VirtualFrame frame) {
		return getSlot().getKind() == FrameSlotKind.Long || getSlot().getKind() == FrameSlotKind.Illegal;
	}
	
	protected boolean isBooleanOrIllegal(VirtualFrame frame) {
		return getSlot().getKind() == FrameSlotKind.Boolean || getSlot().getKind() == FrameSlotKind.Illegal;
	}
	
	
	
}