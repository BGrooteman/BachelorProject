from csv import reader
import numpy as np
import matplotlib.pyplot as plt

benchmark_names = ["While", "PrimeFinder", "GCD", "Fibonacci", "Factorial", "FactorCounter"]
for name in benchmark_names:
	path_jessel = "./Jessel/" + name + ".csv"
	path_recaf = "./Recaf/" + name + ".csv"
	with open(path_jessel, 'r') as f_j, open (path_recaf, 'r') as f_r:
		jessel_data = list(reader(f_j))
		recaf_data	= list(reader(f_r))


	plt.subplots(nrows = 2, ncols = 2)
	plt.autumn()

	ax1 = plt.subplot(2, 2, 1)
	y_pos = np.arange(2)
	bar_width = 0.35
	data = [jessel_data[1][-1], recaf_data[1][-1]]
	data = list(map(int, data))
	data = list(map(lambda x: x/1000000000, data))
	names = ["Jessel", "Recaf"]
	ax1.bar(y_pos, data, alpha = 0.5)
	plt.xticks(y_pos, names)
	plt.ylabel('time ($s$)')
	ax1.set_title('Run 1 repeated 75 times', fontstyle='italic')	


	ax2 = plt.subplot(2, 2, 2)
	y_pos = np.arange(2)
	bar_width = 0.35
	data = [jessel_data[2][-1], recaf_data[2][-1]]
	data = list(map(int, data))
	data = list(map(lambda x: x/1000000000, data))
	names = ["Jessel", "Recaf"]
	ax2.bar(y_pos, data, alpha = 0.5)
	plt.xticks(y_pos, names)
	plt.ylabel('time ($s$)')
	ax2.set_title('Run 2 repeated 75 times', fontstyle='italic')

	ax3 = plt.subplot(2, 2, 3)
	y_pos = np.arange(2)
	bar_width = 0.35
	data = [jessel_data[3][-1], recaf_data[3][-1]]
	data = list(map(int, data))
	data = list(map(lambda x: x/1000000000, data))
	names = ["Jessel", "Recaf"]
	ax3.bar(y_pos, data, alpha = 0.5)
	plt.xticks(y_pos, names)
	plt.ylabel('time ($s$)')
	ax3.set_title('Run 3 repeated 75 times', fontstyle='italic')

	ax4 = plt.subplot(2, 2, 4)
	y_pos = np.arange(2)
	bar_width = 0.35
	data = [jessel_data[4][-1], recaf_data[4][-1]]
	data = list(map(int, data))
	data = list(map(lambda x: x/1000000000, data))
	names = ["Jessel", "Recaf"]
	ax4.bar(y_pos, data, alpha = 0.5)
	plt.xticks(y_pos, names)
	plt.ylabel('time ($s$)')
	ax4.set_title('Run 4 repeated 75 times', fontstyle='italic')

	plt.suptitle("Results for " + name + " benchmark")
	plt.tight_layout()
	plt.subplots_adjust(top=0.88)

	fileName = name + "barplot.png"
	print(fileName)
	plt.savefig(fileName)