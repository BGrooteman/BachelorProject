package com.BScProject.truffle.jsl.runtime;

import com.BScProject.truffle.jsl.JSLException;
import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;

public final class JSLUndefinedNameException extends JSLException {
	
	private final static long serialVersionUID = 1L;
	
	@TruffleBoundary
	public static JSLUndefinedNameException undefinedFunction(Object name) {
		throw new JSLUndefinedNameException("Undefined function: " + name);
	}
	
    @TruffleBoundary
    public static JSLUndefinedNameException undefinedProperty(Object name) {
        throw new JSLUndefinedNameException("Undefined property: " + name);
    }

    private JSLUndefinedNameException(String message) {
        super(message);
    }
	
}