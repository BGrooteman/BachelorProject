package com.oracle.truffle.pl.nodes.call;

import com.oracle.truffle.api.dsl.Cached;
import com.oracle.truffle.api.dsl.Fallback;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.dsl.TypeSystemReference;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.ArityException;
import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.Message;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.interop.UnsupportedMessageException;
import com.oracle.truffle.api.interop.UnsupportedTypeException;
import com.oracle.truffle.api.nodes.DirectCallNode;
import com.oracle.truffle.api.nodes.IndirectCallNode;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.pl.nodes.PLTypes;
import com.oracle.truffle.pl.nodes.interop.PLForeignToPLTypeNode;
import com.oracle.truffle.pl.nodes.interop.PLForeignToPLTypeNodeGen;
import com.oracle.truffle.pl.runtime.PLFunction;
import com.oracle.truffle.pl.runtime.PLUndefinedNameException;

@TypeSystemReference(PLTypes.class)
public abstract class PLDispatchNode extends Node {
	
	protected static final int INLINE_CACHE_SIZE = 2;
	
	public abstract Object executeDispatch(VirtualFrame frame, Object function, Object[] arguments);
	
	// Read the Github comments for more information on this part
	@Specialization(limit = "INLINE_CACHE_SIZE", //
					guards = "function == cachedFunction", //
					assumptions = "cachedFunction.getCallTargetStable()")
	protected static Object doDirect(VirtualFrame frame, PLFunction function, Object[] arguments,
					@Cached("function") PLFunction cachedFunction,
					@Cached("create(cachedFunction.getCallTarget())") DirectCallNode callNode) {
		
		return callNode.call(frame, arguments);
	}
	
	// When we can no longer inline, use this when we reach CACHE_SIZE
	@Specialization(replaces = "doDirect")
	protected static Object doIndirect(VirtualFrame frame, PLFunction function, Object[] arguments,
					@Cached("create()") IndirectCallNode callNode) {
		
		// Lookup the function and call it
		return callNode.call(frame, function.getCallTarget(), arguments);
	}
	
	@Fallback
	protected static Object unknownFunction(Object function, @SuppressWarnings("unused") Object[] arguments) {
		throw PLUndefinedNameException.undefinedFunction(function);
	}
	
	//////Language Interoperability ////////
	@Specialization(guards = "isForeignFunction(function)")
	protected static Object doForeign(VirtualFrame frame, TruffleObject function, Object[] arguments,
					@Cached("createCrossLanguageCallNode(arguments)") Node crossLanguageCallNode,
					@Cached("createToPLTypeNode()") PLForeignToPLTypeNode toPLTypeNode) {
		try {
			//Perform the call
			Object result = ForeignAccess.sendExecute(crossLanguageCallNode, frame, function, arguments);
			//Convert the result
			return toPLTypeNode.executeConvert(frame, result);
		} catch (ArityException | UnsupportedTypeException | UnsupportedMessageException e) {
			// The execute was not successful
			throw PLUndefinedNameException.undefinedFunction(function);
		}
	}
	
	protected static boolean isForeignFunction(TruffleObject function) {
		return !(function instanceof PLFunction);
	}
	
	protected static Node createCrossLanguageCallNode(Object[] arguments) {
		return Message.createExecute(arguments.length).createNode();
	}
	
	protected static PLForeignToPLTypeNode createToPLTypeNode() {
		return PLForeignToPLTypeNodeGen.create();
	}	
}





