package com.BScProject.truffle.jsl.runtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.BScProject.truffle.jsl.nodes.JSLRootNode;
import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.api.Truffle;

/**
 * Manages the mapping from function names to JSLFunction functions.
 */
public final class JSLFunctionRegistry  {
	private final HashMap<String, JSLFunction> functions = new HashMap<>();
	
	/**
	 * Returns the JSLFunction object for the given name. If the name does not exsist yet, it is created
	 */
	public JSLFunction lookup(String name, boolean createIfNotPresent) {
		JSLFunction result = functions.get(name);
		if (result == null && createIfNotPresent) {
			result = new JSLFunction(name);
			functions.put(name, result);
		}
		return result;
	}
	
	
	public JSLFunction register(String name, JSLRootNode rootNode) {
		JSLFunction function = lookup(name, true);
		RootCallTarget callTarget = Truffle.getRuntime().createCallTarget(rootNode);
		function.setCallTarget(callTarget);
		return function;
	}
	
	public void register(Map<String, JSLRootNode> newFunctions) {
		for(Map.Entry<String, JSLRootNode> entry : newFunctions.entrySet()) {
			register(entry.getKey(), entry.getValue());
		}
	}
	
	
	/**
	 * Printing purposes. List of all functions
	 */
	public List<JSLFunction> getAllFunctions() {
		ArrayList<JSLFunction> allFunctions = new ArrayList<>(functions.values());
		Collections.sort(allFunctions, new Comparator<JSLFunction>(){
			public int compare(JSLFunction f1, JSLFunction f2) {
                return f1.toString().compareTo(f2.toString());
            }
        });
		return allFunctions;
	}
	
	
}