package com.BScProject.truffle.jsl.runtime;

import com.oracle.truffle.api.nodes.Node;

public class JSLRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public JSLRuntimeException(Node node, String arg0, Throwable arg1) {
		super(nodeDescription(node) + arg0, arg1);
	}
	
	public JSLRuntimeException(Node node, String arg0) {
		super(nodeDescription(node) + arg0);
	}
	
	public JSLRuntimeException(Node node, Throwable arg0) {
		super(nodeDescription(node) + arg0.getMessage(), arg0);
	}
	
	private static String nodeDescription(Node node) {
		if (node.getSourceSection() == null || node.getSourceSection().getSource() == null) {
			return "<unknown>: ";
		}
		return node.getSourceSection().getSource().getName()+":"+node.getSourceSection().getStartLine()+": "+node.getSourceSection().getCode()+": ";
	}
}