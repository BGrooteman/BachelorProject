package com.oracle.truffle.pl.builtins;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.Message;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.NodeInfo;

@NodeInfo(shortName = "isNull")
public abstract class PLIsNullBuiltin extends PLBuiltinNode {
	
	@Child private Node isNull = Message.IS_NULL.createNode();
	
	@Specialization
	public Object isNull(VirtualFrame frame, TruffleObject obj){
		return ForeignAccess.sendIsNull(isNull, frame, obj);
	}
}