package com.oracle.truffle.pl.builtins;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.pl.PLLanguage;
import com.oracle.truffle.pl.parser.Parser;

@NodeInfo(shortName = "defineFunction")
public abstract class PLDefineFunctionBuiltin extends PLBuiltinNode {
	
	@TruffleBoundary
	@Specialization
	public String defineFunction(String code){
		Source source = Source.newBuilder(code).
				name("[defineFunction]").
				mimeType(PLLanguage.MIME_TYPE).
				build();
		
		getContext().getFunctionRegistry().register(Parser.parsePL(source));
		
		return code;
	}
}