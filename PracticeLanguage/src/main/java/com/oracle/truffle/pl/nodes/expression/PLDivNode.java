package com.oracle.truffle.pl.nodes.expression;

import java.math.BigInteger;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLBinaryNode;

@NodeInfo(shortName = "/")
public abstract class PLDivNode extends PLBinaryNode {
	
	@Specialization(rewriteOn = ArithmeticException.class)
	public long div(long left, long right) throws ArithmeticException{
		long result = left/right;
		if((left & right & result) < 0) {
			throw new ArithmeticException("long overflow");
		}
		return result;
	}
	
	
	@Specialization
	@TruffleBoundary
	public BigInteger div(BigInteger left, BigInteger right) {
		return left.divide(right);
	}
}