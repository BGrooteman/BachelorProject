package com.oracle.truffle.pl.nodes.expression;

import java.math.BigInteger;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLBinaryNode;

@NodeInfo(shortName = "<")
public abstract class PLLessThanNode extends PLBinaryNode {
	
	@Specialization
	protected boolean lessThan(long left, long right) {
		return left < right;
	}
	
	@Specialization
	@TruffleBoundary
	protected boolean lessThan(BigInteger left, BigInteger right) {
		return left.compareTo(right) < 0;
	}
	
}