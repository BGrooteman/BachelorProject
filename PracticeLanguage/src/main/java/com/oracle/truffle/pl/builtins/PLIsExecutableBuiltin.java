package com.oracle.truffle.pl.builtins;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.Message;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.NodeInfo;

@NodeInfo(shortName = "isExecutable")
public abstract class PLIsExecutableBuiltin extends PLBuiltinNode {
	
	@Child private Node isExecutable = Message.IS_EXECUTABLE.createNode();
	
	@Specialization
	public Object isExecutable(VirtualFrame frame, TruffleObject obj) {
		return ForeignAccess.sendIsExecutable(isExecutable, frame, obj);
	}
}