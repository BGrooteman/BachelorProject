package com.oracle.truffle.pl.nodes.expression;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

@NodeInfo(shortName = "const")
public final class PLLongLiteralNode extends PLExpressionNode {
	
	private final long value;
	
	public PLLongLiteralNode(long value) {
		this.value = value;
	}
	
	@Override
	public long executeLong(VirtualFrame frame) {
		return value;
	}
	
	@Override
	public Object executeGeneric(VirtualFrame frame) {
		return value;
	}
}