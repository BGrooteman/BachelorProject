from csv import reader
import matplotlib.pyplot as plt

plt.style.use('ggplot')

plt.title("Some title", fontname='Ubuntu', fontsize=14,
            fontstyle='italic', fontweight='bold')

benchmark_platform = ["Jessel", "Recaf"]
benchmark_names = ["While", "PrimeFinder", "GCD", "Fibonacci", "Factorial", "FactorCounter"]
for platform in benchmark_platform:
	for name in benchmark_names:
		path = "./" + platform + "/" + name + ".csv"
		with open(path, 'r') as f:
			data = list(reader(f))
			

		#Four plots, with shared x axis for the runtimes
		f, axarr = plt.subplots(4, sharex=True)
		plt.xlabel("Runs")
		plt.ylabel("Time (nsec)")
		for i in range(4):
			run_times = data[i+1]
			run_times = run_times[1:-1]

			axarr[i].scatter(range(len(run_times)), run_times, label=platform)
			# plt.scatter(range(len(run1_recaf)), run1_recaf, marker="s", facecolors = "None", color="black", s=30, linewidths=1, label="Recaf")
			# plt.set_yscale('log')
			# plt.xlim((np.min(x), np.max(x)))
			# plt.ylim((np.min(y), np.max(y)))
			axarr[i].set_title("Input " + str(i+1))
			axarr[i].legend(scatterpoints=1)

		
		plt.suptitle("Each run for " + name + " benchmark")
		plt.tight_layout()
		plt.subplots_adjust(top=0.88)

		fileName = name + platform + ".png"
		print(fileName)
		plt.savefig(fileName)