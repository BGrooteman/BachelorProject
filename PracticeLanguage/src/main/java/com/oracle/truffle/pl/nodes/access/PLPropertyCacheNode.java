package com.oracle.truffle.pl.nodes.access;

import java.math.BigInteger;

import com.oracle.truffle.api.CompilerAsserts;
import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.TypeSystemReference;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.object.DynamicObject;
import com.oracle.truffle.api.object.Shape;
import com.oracle.truffle.pl.nodes.PLTypes;
import com.oracle.truffle.pl.runtime.PLContext;
import com.oracle.truffle.pl.runtime.PLFunction;
import com.oracle.truffle.pl.runtime.PLNull;

@TypeSystemReference(PLTypes.class)
public abstract class PLPropertyCacheNode extends Node {
	protected static final int CACHE_LIMIT = 3;
	
	protected static boolean shapeCheck(Shape shape, DynamicObject receiver) {
		return shape != null && shape.check(receiver);
	}
	
	protected static Shape lookupShape(DynamicObject receiver) {
		CompilerAsserts.neverPartOfCompilation();
		
		if(!PLContext.isPLObject(receiver)) {
			// This is done by the doForeignObject specialization
			return null;
		}
		return receiver.getShape();
	}
	
	protected static boolean isValidPLObject(DynamicObject receiver) {
		if(!PLContext.isPLObject(receiver)) {
			// This is done by the doForeignObject specialization
			return false;
		}
		return receiver.getShape().isValid();
	}
	
	protected static boolean isForeignObject(TruffleObject receiver) {
		return !PLContext.isPLObject(receiver);
	}
	
	//// Check github of simplelanguage for good explaination on how this works
	// Check if is same type, and then compare
	protected static boolean namesEqual(Object cachedName, Object name) {
		if(cachedName instanceof Long && name instanceof Long) {
			return (long) cachedName == (long) name;
		} else if (cachedName instanceof BigInteger && name instanceof BigInteger) {
			return equalBigInteger((BigInteger)cachedName, (BigInteger) name);
		} else if (cachedName instanceof Boolean && name instanceof Boolean) {
			return (boolean) cachedName == (boolean) name;
		} else if (cachedName instanceof String && name instanceof String) {
			return ((String)cachedName).equals(name);
		} else if (cachedName instanceof PLFunction && name instanceof PLFunction) {
			return cachedName == name;
		} else if (cachedName instanceof PLNull && name instanceof PLNull) {
			return cachedName == name;
		} else {
			assert !cachedName.equals(name);
			return false;
		}
	}
	
	@TruffleBoundary
	private static boolean equalBigInteger(BigInteger left, BigInteger right) {
		return left.equals(right);
	}
	
}