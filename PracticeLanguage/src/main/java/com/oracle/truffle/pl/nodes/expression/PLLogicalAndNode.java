package com.oracle.truffle.pl.nodes.expression;

import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

@NodeInfo(shortName = "&&")
public final class PLLogicalAndNode extends PLShortCircuitNode {
	
	public PLLogicalAndNode(PLExpressionNode left, PLExpressionNode right){
		super(left, right);
	}
	
	@Override
	protected boolean isEvaluateRight(boolean left) {
		return false;
	}
	
	@Override
	protected boolean execute(boolean left, boolean right) {
		return left && right;
	}
}