package com.oracle.truffle.pl.nodes.expression;

import java.math.BigInteger;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.ExactMath;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLBinaryNode;

// The Node that impelements the + for abitrary precision numbers.
// Also String concat

@NodeInfo(shortName = "+")
public abstract class PLAddNode extends PLBinaryNode {
	
	
	// Specialization for long values
	@Specialization(rewriteOn = ArithmeticException.class)
	protected long add(long left, long right) {
		//Throws ArithmeticException when overflow occurs
		return ExactMath.addExact(left, right);
	}
	
	
	//Specialization for BiGIntegers
	@Specialization
	@TruffleBoundary
	protected BigInteger add(BigInteger left, BigInteger right) {
		return left.add(right);
	}
	
	
	//Specialization for string concatination
	@Specialization(guards = "isString(left, right)")
	@TruffleBoundary
	protected String add(Object left, Object right) {
		return left.toString() + right.toString();
	}
	
	protected boolean isString(Object left, Object right) {
		return left instanceof String || right instanceof String;
	}
	
}