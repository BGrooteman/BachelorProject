package com.oracle.truffle.pl.runtime;

import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.object.DynamicObject;
import com.oracle.truffle.api.object.ObjectType;

public final class PLObjectType extends ObjectType {
	public static final ObjectType SINGLETON = new PLObjectType();
	
	private PLObjectType() {
	}
	
	public static boolean isInstance(TruffleObject obj) {
		return PLContext.isPLObject(obj);
	}
	
	@Override
	public ForeignAccess getForeignAccessFactory(DynamicObject obj) {
		return PLObjectMessageResolutionForeign.createAccess();
	}
}