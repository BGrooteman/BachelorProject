package com.oracle.truffle.pl.runtime;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.pl.PLException;

public final class PLUndefinedNameException extends PLException {
	private static final long serialVersionUID = 1L;
	
	@TruffleBoundary
	public static PLUndefinedNameException undefinedFunction(Object name) {
		throw new PLUndefinedNameException("Undefined function: " + name);
	}
	
	@TruffleBoundary
	public static PLUndefinedNameException undefinedProperty(Object name) {
		throw new PLUndefinedNameException("Undefined property: " + name);
	}
	
	private PLUndefinedNameException(String message) {
		super(message);
	}
}