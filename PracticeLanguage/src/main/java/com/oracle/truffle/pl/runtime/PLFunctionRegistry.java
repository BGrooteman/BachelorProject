package com.oracle.truffle.pl.runtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.pl.nodes.PLRootNode;

// Maps function names to function objects
public final class PLFunctionRegistry {
	private final Map<String, PLFunction> functions = new HashMap<>();
	
	// Returns the canonical PLFunction Object for given name
	public PLFunction lookup(String name, boolean createIfNotPresent) {
		PLFunction result = functions.get(name);
		if(result == null && createIfNotPresent) {
			result = new PLFunction(name);
			functions.put(name, result);
		}
		return result;
	}
	
	
	// Associates the PLFunction with the given name to a implementation node
	public PLFunction register(String name, PLRootNode rootNode){
		PLFunction function = lookup(name, true);
		RootCallTarget callTarget = Truffle.getRuntime().createCallTarget(rootNode);
		function.setCallTarget(callTarget);
		return function;
	}
	
	public void register(Map<String, PLRootNode> newFunctions){
		for(Map.Entry<String, PLRootNode> entry : newFunctions.entrySet()){
			register(entry.getKey(), entry.getValue());
		}
	}
	
	//Gets list of all registered functions. This is solely used for printing
	public List<PLFunction> getFunctions() {
		List<PLFunction> result = new ArrayList<>(functions.values());
		Collections.sort(result, new Comparator<PLFunction>() {
			public int compare(PLFunction f1, PLFunction f2) {
				return f1.toString().compareTo(f2.toString());
			}
		});
		return result;
	}
	
}