package com.oracle.truffle.pl.nodes.controlflow;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.oracle.truffle.api.CompilerAsserts;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.ExplodeLoop;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLStatementNode;

@NodeInfo(shortName = "block", description = "The node implementing a source code block")
public final class PLBlockNode extends PLStatementNode {
	
	//Array of child nodes. @Children tells Truffle that there are multiple children
	//Required to be a final array of nodes.
	@Children private final PLStatementNode[] bodyNodes;
	
	public PLBlockNode(PLStatementNode[] bodyNodes) {
		this.bodyNodes = bodyNodes;
	}
	
	
	//Execute all children. @ExplodeLoop unrolls the whole loop
	//This allows the loop to be inlined == Speed
	@Override
	@ExplodeLoop
	public void executeVoid(VirtualFrame frame) {
		// Assertion to show that array length is really constant
		// during compilation
		CompilerAsserts.compilationConstant(bodyNodes.length);
		
		for(PLStatementNode statement : bodyNodes) {
			statement.executeVoid(frame);
		}
	}
	
	public List<PLStatementNode> getStatements() {
		return Collections.unmodifiableList(Arrays.asList(bodyNodes));
	}
	
}