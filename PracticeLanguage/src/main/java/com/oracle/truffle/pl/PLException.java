package com.oracle.truffle.pl;

import java.util.ArrayList;
import java.util.List;

import com.oracle.truffle.api.CompilerAsserts;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.frame.FrameInstance;
import com.oracle.truffle.api.frame.FrameInstanceVisitor;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.RootNode;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.pl.nodes.PLRootNode;

// Since we do not need a sophisticated error checking. We just abort execution 
// when an exception is encountered
public class PLException extends RuntimeException {
	private static final long serialVersionUID = 3373249185137396147L;
	
	public PLException(String message) {
		super(message);
		CompilerAsserts.neverPartOfCompilation();
		initCause(new Throwable("Java stack trace"));
	}
	
	@Override
	@SuppressWarnings("sync-override")
	public Throwable fillInStackTrace() {
		CompilerAsserts.neverPartOfCompilation();
		return fillInPLStackTrace(this);
	}

	// Use the Truffle API tp oterate the stack frames and set the proper elements
	public static Throwable fillInPLStackTrace(Throwable t) {
		final List<StackTraceElement> stackTrace = new ArrayList<>();
		Truffle.getRuntime().iterateFrames(new FrameInstanceVisitor<Void>(){
			public Void visitFrame(FrameInstance frame) {
				Node callNode = frame.getCallNode();
				if (callNode == null) {
					return null;
				}
				RootNode root = callNode.getRootNode();
				
				//There should be no other Rootnodes other rhan PLRootNodes.
				// But just to be sure
				String methodName = "$unknownFunction";
				if (root instanceof PLRootNode) {
					methodName = ((PLRootNode) root).getName();
				}
				
				SourceSection sourceSection = callNode.getEncapsulatingSourceSection();
				Source source = sourceSection != null ? sourceSection.getSource() : null;
				String sourceName = source != null ? source.getName() : null;
				int lineNumber;
				try {
					lineNumber = sourceSection != null ? sourceSection.getStartLine() : -1;
				} catch (UnsupportedOperationException e) {
					lineNumber = -1;
				}
				stackTrace.add(new StackTraceElement("PL", methodName, sourceName, lineNumber));
				return null;
			}
		});
		t.setStackTrace(new StackTraceElement[stackTrace.size()]);
		return t;
	}
	
	
	
}
