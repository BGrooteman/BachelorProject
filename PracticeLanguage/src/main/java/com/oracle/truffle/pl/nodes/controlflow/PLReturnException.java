package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.nodes.ControlFlowException;

public final class PLReturnException extends ControlFlowException {
	private static final long serialVersionUID = 1L;
	
	private final Object result;
	
	public PLReturnException(Object result) {
		this.result = result;
	}
	
	public Object getResult(){
		return result;
	}
}