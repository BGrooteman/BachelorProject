package com.oracle.truffle.pl.nodes.expression;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

@NodeInfo(shortName = "const")
public final class PLStringLiteralNode extends PLExpressionNode {
	private final String value;
	
	public PLStringLiteralNode(String value) {
		this.value = value;
	}
	
	@Override
	public String executeGeneric(VirtualFrame frame) {
		return value;
	}
}