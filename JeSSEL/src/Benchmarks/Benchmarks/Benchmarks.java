package Benchmarks;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import Benchmarks.BenchmarkData.Benchmark;

public class Benchmarks {	
	private static void runOnGraal(String benchmarkName, int runNumber, int warmupRuns, int actualRuns, String resultFilePath) throws IOException, InterruptedException {
	    String separator = System.getProperty("file.separator");
	    String classpath = System.getProperty("java.class.path");
	    //TODO Niet dit....eigenlijk
	    String path = "/home/bram/Documents/Uni/y3/BachelorProject/BachelorProject/JeSSEL/graalvm/bin/"+ separator + "java";
	    ProcessBuilder processBuilder = 
	                new ProcessBuilder(path, "-cp", 
	                classpath, 
	                JeSSELBenchmarkRunner.class.getName(), benchmarkName, Integer.toString(runNumber), Integer.toString(warmupRuns), Integer.toString(actualRuns), resultFilePath);
	    Process process = processBuilder.inheritIO().start();
	    process.waitFor();
	    process.destroy();
	}
	
	private static void runOnJVM(String benchmarkName, int runNumber, int warmupRuns, int actualRuns, String resultFilePath) throws IOException, InterruptedException {
	    String separator = System.getProperty("file.separator");
	    String classpath = System.getProperty("java.class.path");
	    String path = System.getProperty("java.home")
	                + separator + "bin" + separator + "java";
	    ProcessBuilder processBuilder = 
	                new ProcessBuilder(path, "-cp", 
	                classpath, 
	                RecafBenchmarkRunner.class.getName(), benchmarkName, Integer.toString(runNumber), Integer.toString(warmupRuns), Integer.toString(actualRuns), resultFilePath);
	    Process process = processBuilder.inheritIO().start();
	    process.waitFor();
	    process.destroy();
	}
	
	private static void printLine(BufferedWriter f, String... args){
		int len = args.length;
		if (len <= 0){
			return;
		}
		try {
			for(int i = 0; i < len-1; i++) {
				f.append(args[i]).append(',');
			}
			f.append(args[len-1]).append('\n');
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	// Setup the file to record the results
	private static void setupFile(Benchmark benchmark, int actualRuns, File f) {
		BufferedWriter out;
		try {
			out = new BufferedWriter(new FileWriter(f, false));
			String[] strArr = new String[actualRuns+2];
			strArr[0] = benchmark.getName();
			for(int i=0;i<actualRuns;i++) {
				strArr[i+1] = "Run " + String.valueOf(i);
			}
			strArr[actualRuns + 1] = "Total time";
			printLine(out, strArr);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void runOnVM(Benchmark benchmark, int runNumber, int warmupRuns, int actualRuns, String jesselFilePath, String recafFilePath) {
		try {
			runOnGraal(benchmark.getName(), runNumber, warmupRuns, actualRuns, jesselFilePath);
			runOnJVM(benchmark.getName(), runNumber, warmupRuns, actualRuns, recafFilePath);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	/* We want to run benchmarks on a clean machine (that only runs the benchmark in this fashion:
	 * [For each benchmark-run]
	 * 		- Start VM
	 * 		- Run warmup x times
	 * 		- Run benchmark and time y times
	 * 		- write results to file
	 * 		- Stop vm
	 */
	public static void main(String args[]) {
		// Load in Benchmarks and run it using the runner.		
		System.out.println("Found " + BenchmarkData.getBenchmarks().size() + " benchmarks:");
		BenchmarkData.getBenchmarks().forEach(x -> System.out.println("   - " + x.getName()));
		
		int warmupRuns = 50;
		int actualRuns = 75;
		
		for(Benchmark benchmark : BenchmarkData.getBenchmarks()) {
			File resultFileJeSSEL = new File("../BenchmarkResults/Jessel/" + benchmark.getName() + "OnGraal.csv");
			File resultFileRecaf = new File("../BenchmarkResults/Recaf/" + benchmark.getName() + "OnGraal.csv");
			setupFile(benchmark, actualRuns, resultFileJeSSEL);
			setupFile(benchmark, actualRuns, resultFileRecaf);
			
			// Run each benchmark runNr times on both VM's
			for(int runNr = 0; runNr < benchmark.getNrRuns(); runNr++) {
				runOnVM(benchmark, runNr, warmupRuns, actualRuns, resultFileJeSSEL.getAbsolutePath(), resultFileRecaf.getAbsolutePath());
			}
		}		
	}
}