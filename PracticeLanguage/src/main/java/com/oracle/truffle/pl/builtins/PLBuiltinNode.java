package com.oracle.truffle.pl.builtins;

import com.oracle.truffle.api.dsl.GenerateNodeFactory;
import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.NodeField;
import com.oracle.truffle.pl.nodes.PLExpressionNode;
import com.oracle.truffle.pl.runtime.PLContext;

@NodeChild(value = "arguments", type = PLExpressionNode[].class)
@NodeField(name = "context", type = PLContext.class)
@GenerateNodeFactory
public abstract class PLBuiltinNode extends PLExpressionNode {
	// accessor for the PLContext
	public abstract PLContext getContext();
}