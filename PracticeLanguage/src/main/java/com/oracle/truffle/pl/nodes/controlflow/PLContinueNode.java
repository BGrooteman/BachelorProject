package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLStatementNode;

@NodeInfo(shortName = "continue", description = "The node implementing a continue statement")
public final class PLContinueNode extends PLStatementNode {
	
	@Override
	public void executeVoid(VirtualFrame frame) {
		throw PLContinueException.INSTANCE;
	}
}