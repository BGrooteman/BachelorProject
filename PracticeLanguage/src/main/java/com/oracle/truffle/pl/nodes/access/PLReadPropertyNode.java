package com.oracle.truffle.pl.nodes.access;

import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.NodeChildren;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLExpressionNode;

// Node for reading property of object. On execution this node
// Evaluates the expression on the lhs of the object access operator
// evaluates the property name
// reads the named property

@NodeInfo(shortName = ".")
@NodeChildren({@NodeChild("receiverNode"), @NodeChild("nameNode")})
public abstract class PLReadPropertyNode extends PLExpressionNode {
	
	
	// The polymorphic cachenode that actually performs the read.
	@Child private PLReadPropertyCacheNode readNode = PLReadPropertyCacheNodeGen.create();
	
	@Specialization
	protected Object read(VirtualFrame frame, Object receiver, Object name) {
		return readNode.executeRead(frame, receiver, name);
	}
}