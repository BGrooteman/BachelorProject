\chapter{Benchmark design}
The cause for this project was, as stated before, to see if Truffle could provide a better performing backend to the Recaf project. Since the project time limited me to implement just the basics of the JeSSEL language, really comparing the performance of Recaf against Recaf on Truffle is not (yet) possible. We can however, see how the JeSSEL language performs in comparison with the Recaf interpreter. To do so, six benchmarks are ran both on Recaf and JeSSEL. In this chapter is explained how these benchmarks were set up.
\section{Requirements}
Getting good unbiased results from benchmarks has proven to be a task that requires some consideration. To make sure the performance is properly measured some requirements for the benchmarks were put in place. 
\begin{itemize}
\item Reproducibility: The benchmarks should be set up in such a way that the results can be easily reproduced/verified.
\item Uniformity: The benchmarks should be ran in the same environment, under the same conditions. The way of measuring time should be the same and independent of the language implementation. 
\item Accuracy: The time that is measured should be sufficiently accurate to distinguish the languages. Nanoseconds should be used.
\end{itemize}
As shown in the `Virtual Machine Warmup Blows Hot and Cold` paper by Edd Barrett et al. \citep{warmup} on benchmarking and the importance of properly warming up the VM before running the benchmarks is of significant importance. In the setup section is explained how these requirements are implemented in the actual benchmarks.\\

To test the performance of JeSSEL the benchmarks need to test several aspects of the language. We especially focus on micro benchmarks due to the fact that JeSSEL does not have classes/objects, nor any way of making modules of the code. This would make implementing a larger program/macro benchmarks challenging. The main aspects the benchmarks should focus on are: \begin{compactitem}
\item Basic operations
\item Controlflow such as If and While statements
\end{compactitem}
Based on these focus points six benchmarks were selected.
\section{Benchmarks}
The benchmarks that were ran are inspired by `the Benchmark game`\citep{BenchmarkGame} and `Are we fast yet`\citep{AWFY}. The other benchmarks I found on several places. All the code for the benchmarks (in .jsl files) can be found in Appendix A.
\begin{compactitem}
\item \textbf{Greatest Common Divisor} Calculating the GCD between every number in a range(\textit{r1}, \textit{r2}) and a fixed number \textit{k}. \\
\textit{Inputs: r1, r2, k} 
\item \textbf{Factorial} Calculating the factorial of \textit{f}, \textit{size} times. Implemented in an iterative fashion. \\
\textit{Inputs: f, size.}
\item \textbf{PrimeFinder} This benchmark counts the number of prime numbers within a certain range up to \textit{size}. It does this by for every number in the range performing a loop over [0, num/2] and testing if we can find a factor.\\
\textit{Inputs: size}
\item \textbf{Fibonnaci} Calculation of the fibonacci sequence up to \textit{f}, \textit{n} times. Implemented in an iterative fashion.\\
\textit{Inputs: f, n1, n2, size}
\item \textbf{FactorCounter} This benchmark counts the number of proper factors for every number in the range [0, size]. Does this by having a double nested loop with an if statement that checks if we found a proper factor. \\
\textit{Inputs: size}
\item \textbf{While} A simple doubly nested while loop that does not really calculate something. To test how well the platforms handle big nested loops.\\
\textit{Inputs: size}
\end{compactitem}
\newpage

\section{Setup}
All the benchmarks are run on an ASUS laptop running Ubuntu 16.04, with 8 Cores at 2.00GHz and with 6GB of RAM. \\
I built a small environment that makes it easy to run the benchmarks. This environment is needed because the results need to be reproducible. As for the benchmarks, we want a structure where every new input is run from an empty cold VM, to ensure the uniformity requirement. Another reason for buildin the program is that doing this manually would become a lot of work/ a big inconvenience.
The figure below shows how that is setup.
\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{Figures/benchmarksetup.png}
\caption{How the benchmark are setup}
\end{figure}
\\
\textbf{BenchmarkData} This is a static class that holds all the benchmark data. It contains a list of Benchmark's which can then be used by the runners to get the run details for the benchmarks. Every benchmark extends the Benchmark class, which makes it easy to extract all the data in the runners.\\
\textbf{Benchmarks} This is the main class, from here the VM's are booted up for running. This happens for every different input of every benchmark. The Benchmark itself runs on the JVM and uses the ProcessBuilder to start new (cold) instances of the GraalVM and the JVM. Both the VM's are ran without any extra parameters:
\begin{lstlisting}
java RecafBenchmarkRunner
\end{lstlisting}
\begin{lstlisting}
./graal/java JesselBenchmarkRunner
\end{lstlisting}
\textbf{Runners} The runners have the language-specific code that runs on the VM. Firstly the runners parse the benchmark into a runnable AST, this parsing process should not  be part of the benchmark. The runner then runs this parsed source 50 times to warm up the VM, these runs are also not part of the measurements. When the VM is warmed up we run the parsed source again 75 times, but now each run is timed with System.nanoTime(). The time for each run is recorded and after the measurements written to a .csv file for later analysis.\\
The upside to this setup is that all the benchmark specific information, like the input values and number of repetitions are not intertwined with the code that actual runs the benchmarks. This makes it so that it is easy to change these values, add other benchmarks or verify results.\\
After all the benchmarks are run, the .csv files were to create tables and graphs from the results. Python with Matlibplot was used for creating the different plots and \href{www.tablesgenerator.com/latex_tables}{LateX table generator} was used to create the tables.
Three different plots are included, a scatter plot showing the time each run took, a barplot to compare the total runtime of both VM's and a graph that showed the relative speedup of JeSSel to Recaf.

\subsection{Recaf}
The whole goal of this project was to test if JeSSEL would be a better performing backend than the current interpreter of Recaf. As shown above we designed benchmarks to test the performance. To make sure we run the same code on Recaf as we run on Truffle, a parser was written to parse the JeSSEL files into an Object Algebra that can run on the Recaf interpreter. By doing this it was ensured that the front-end of Recaf did not introduce shortcuts or workarounds, and we really test the performance of the two interpreters. This parser for Recaf was also written using \href{github.com/javaparser/javaparser}{JavaParser}.
For the benchmarks a version of Recaf was used that did not contain higher order abstract syntax (HOAS), but instead used an Environment that contained the variables. The result of the parsing process is an IExecEnv that can be called and has to be supplied a label and an empty Environment.\\\\
\textbf{Limitations of Recaf} \\
The benchmarks are written so that they can both be ran on JeSSEL as well as Recaf. Recaf did pose some extra limitations on the 
- No arrays \\
- Typesafe, but due to the fact that JeSSEL has (automatic) casting this caused problems. This means we can write `double d = 1 + 2.4` in JeSSEL but not in Recaf.\\
- No real functions, and therefore also no way of passing arguments to the main function. \\
This last limitation posed some problems for the benchmarks, since there needed to be runvalues inserted into the program. I solved this by adding the initial values in the Environment before starting the actual execution. This way the variables could be accessed during execution.

