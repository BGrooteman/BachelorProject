package com.oracle.truffle.pl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Map;

import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.CompilerAsserts;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.TruffleLanguage;
import com.oracle.truffle.api.debug.DebuggerTags;
import com.oracle.truffle.api.instrumentation.ProvidedTags;
import com.oracle.truffle.api.instrumentation.StandardTags;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.pl.nodes.PLEvalRootNode;
import com.oracle.truffle.pl.nodes.PLRootNode;
import com.oracle.truffle.pl.runtime.PLContext;
import com.oracle.truffle.pl.runtime.PLNull;
import com.oracle.truffle.pl.runtime.PLFunction;
import com.oracle.truffle.pl.parser.*;

@TruffleLanguage.Registration(name = "PL", version = "0.1", mimeType = PLLanguage.MIME_TYPE)
@ProvidedTags({StandardTags.CallTag.class, StandardTags.StatementTag.class, StandardTags.RootTag.class, DebuggerTags.AlwaysHalt.class})
public final class PLLanguage extends TruffleLanguage<PLContext> {
    public static final String MIME_TYPE = "application/x-pl";
    
    // Singleton instance of language
    public static final PLLanguage INSTANCE = new PLLanguage();
    
    // Private so only singleton can access.
    private PLLanguage(){
    }
    
    @Override
    protected PLContext createContext(Env env) {
    	BufferedReader in = new BufferedReader(new InputStreamReader(env.in()));
    	PrintWriter out = new PrintWriter(env.out(), true);
    	return new PLContext(env, in, out);
    }
    
    @Override
    protected CallTarget parse (ParsingRequest request) throws Exception {
    	Source source = request.getSource();
    	Map<String, PLRootNode> functions;
    	
    	// Parse the provided source. We do not have a PLContext yet, function registration will
    	// happen lazily in the PLEvalRootNode
    	if(request.getArgumentNames().isEmpty()) {
    		functions = Parser.parsePL(source);
    	} else {
    		StringBuilder sb = new StringBuilder();
    		sb.append("function main(");
    		String sep = "";
    		for (String argumentName : request.getArgumentNames()) {
    			sb.append(sep);
    			sb.append(argumentName);
    			sep = ",";
    		}
    		sb.append(") { return ");
    		sb.append(request.getSource().getCode());
    		sb.append(";}");
    		Source decoratedSource = Source.newBuilder(sb.toString()).mimeType(request.getSource().getMimeType()).name(request.getSource().getName()).build();
    		functions = Parser.parsePL(decoratedSource);
    	}
    	
    	PLRootNode main = functions.get("main");
    	PLRootNode evalMain;
    	if (main != null) {
    		// We have a main function. We want to envoke it but we need it. Therefore make a new
    		// RootNode that has everything, and execute that.
    		
    		evalMain = new PLEvalRootNode(main.getFrameDescriptor(), main.getBodyNode(), main.getSourceSection(), main.getName(), functions);
    	} else {
    		// Even if we do not have a main. We still need the new node
    		evalMain = new PLEvalRootNode(null, null, null, "[no main]", functions);
    	}
    	return Truffle.getRuntime().createCallTarget(evalMain);
    }
    
    @Override
    protected Object findExportedSymbol(PLContext context, String globalName, boolean onlyExplicit) {
    	return context.getFunctionRegistry().lookup(globalName, false);
    }
    
    @Override
    public Object getLanguageGlobal(PLContext context) {
    	// The language does not have global variables, so context is the global function registry
    	return context;
    }
    
    @Override
    protected boolean isObjectOfLanguage(Object object) {
    	return object instanceof PLFunction;
    }
    
    @Override
    protected String toString(PLContext context, Object value){
    	if (value == PLNull.SINGLETON) {
    		return "NULL";
    	}
    	if (value instanceof Long) {
    		return Long.toString((Long) value);
    	}
    	return super.toString(context, value);
    }
    
    @Override
    protected Object findMetaObject(PLContext context, Object value) {
    	if (value instanceof Long || value instanceof BigInteger) {
    		return "Number";
    	}
    	if (value instanceof Boolean) {
    		return "Boolean";
    	}
    	if (value instanceof String) {
    		return "String";
    	}
    	if (value == PLNull.SINGLETON) {
    		return "Null";
    	}
    	if (value instanceof PLFunction) {
    		return "Function";
    	}
    	return "Object";
    }
    
    @Override
    protected SourceSection findSourceLocation(PLContext context, Object value){
    	if (value instanceof PLFunction) {
    		PLFunction f = (PLFunction) value;
    		return f.getCallTarget().getRootNode().getSourceSection();
    	}
    	return null;
    }
    
    //? 
    public PLContext findContext() {
    	CompilerAsserts.neverPartOfCompilation();
    	return super.findContext(super.createFindContextNode());
    }
    
    
    
    
    
    
    
    
}