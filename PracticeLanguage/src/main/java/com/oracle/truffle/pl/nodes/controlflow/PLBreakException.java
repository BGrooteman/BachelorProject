package com.oracle.truffle.pl.nodes.controlflow;

import com.oracle.truffle.api.nodes.ControlFlowException;

//Exception thrown by the PLBreakNode and caught by the PLWhileNode
public final class PLBreakException extends ControlFlowException {
	public static final PLBreakException SINGLETON = new PLBreakException();
	
	private static final long serialVersionUID = 100L;
			
	private PLBreakException() {
	}
}