package com.oracle.truffle.pl.nodes.expression;

import java.math.BigInteger;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.ExactMath;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.pl.nodes.PLBinaryNode;

@NodeInfo(shortName = "*")
public abstract class PLMulNode extends PLBinaryNode {
	
	@Specialization(rewriteOn = ArithmeticException.class)
	protected long mul(long left, long right) {
		return ExactMath.multiplyExact(left, right);
	}
	
	@Specialization
	@TruffleBoundary
	protected BigInteger mul(BigInteger left, BigInteger right) {
		return left.multiply(right);
	}
}