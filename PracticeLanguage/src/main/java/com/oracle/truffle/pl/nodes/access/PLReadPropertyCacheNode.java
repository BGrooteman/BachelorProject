package com.oracle.truffle.pl.nodes.access;

import com.oracle.truffle.api.CompilerAsserts;
import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Cached;
import com.oracle.truffle.api.dsl.Fallback;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.Message;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.interop.UnknownIdentifierException;
import com.oracle.truffle.api.interop.UnsupportedMessageException;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.object.DynamicObject;
import com.oracle.truffle.api.object.Location;
import com.oracle.truffle.api.object.Property;
import com.oracle.truffle.api.object.Shape;
import com.oracle.truffle.pl.nodes.interop.PLForeignToPLTypeNode;
import com.oracle.truffle.pl.nodes.interop.PLForeignToPLTypeNodeGen;
import com.oracle.truffle.pl.runtime.PLUndefinedNameException;

@SuppressWarnings("unused")
public abstract class PLReadPropertyCacheNode extends PLPropertyCacheNode {
	
	public abstract Object executeRead(VirtualFrame frame, Object receiver, Object name);
	
	// Polymorphic inline cache for a limited number of distinct properties/shapes
	@Specialization(limit = "CACHE_LIMIT",
					guards = {
							"namesEqual(cachedName, name)",
							"shapeCheck(shape, receiver)"
					},
					assumptions = {
							"shape.getValidAssumption()"
					})
	protected static Object readCached(DynamicObject receiver, Object name,
					@Cached("name") Object cachedName,
					@Cached("lookupShape(receiver)") Shape shape,
					@Cached("lookupLocation(shape, name)") Location location) {
		return location.get(receiver, shape);
	}
	
	protected static Location lookupLocation(Shape shape, Object name) {
		// Always happen slow-path
		CompilerAsserts.neverPartOfCompilation();
		
		Property property = shape.getProperty(name);
		if (property == null) {
			// Property does not exist
			throw PLUndefinedNameException.undefinedProperty(name);
		}
		return property.getLocation();
	}
	
	// The generic case in which the the number of shapes accessed is overflows the cache
	@TruffleBoundary
	@Specialization(replaces = {"readCached"}, guards = {"isValidPLObject(receiver)"})
	protected static Object readUncached(DynamicObject receiver, Object name) {
		
		Object result = receiver.get(name);
		if(result == null) {
			// We can not find this property
			throw PLUndefinedNameException.undefinedProperty(name);
		}
		return result;
	}
	
	// When no specialization fits, the receiver isnt an Object 
	// Or the Object has a shape that has been invalidated
	@Fallback
	protected static Object updateShape(Object r, Object name){
		// No need to invalidate compiled code
		CompilerDirectives.transferToInterpreter();
		
		if (!(r instanceof DynamicObject)) {
			throw PLUndefinedNameException.undefinedProperty(name);
		}
		DynamicObject receiver = (DynamicObject) r;
		receiver.updateShape();
		return readUncached(receiver, name);
	}
	
	
	/////// Language Interoperability
	@Specialization(guards = "isForeignObject(receiver)")
	protected static Object readForeign(VirtualFrame frame, TruffleObject receiver, Object name,
					@Cached("createForeignReadNode()") Node foreignReadNode,
					@Cached("createToPLTypeNode()") PLForeignToPLTypeNode toPLTypeNode) {
		try {
			Object result = ForeignAccess.sendRead(foreignReadNode, frame, receiver, name);
			return toPLTypeNode.executeConvert(frame, result);
		} catch (UnknownIdentifierException | UnsupportedMessageException e){
			throw PLUndefinedNameException.undefinedProperty(name);
		}
	}
	
	protected static Node createForeignReadNode() {
		return Message.READ.createNode();
	}
	
	protected static PLForeignToPLTypeNode createToPLTypeNode() {
		return PLForeignToPLTypeNodeGen.create();
	}
	
	
	
	
	
	
}