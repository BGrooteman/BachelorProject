package com.oracle.truffle.pl.builtins;


import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.interop.ForeignAccess;
import com.oracle.truffle.api.interop.Message;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.NodeInfo;

@NodeInfo(shortName = "hasSize")
public abstract class PLHasSizeBuiltin extends PLBuiltinNode {
	
	@Child private Node hasSize = Message.HAS_SIZE.createNode();
	
	@Specialization
	public Object hasSize(VirtualFrame frame, TruffleObject obj) {
		return ForeignAccess.sendHasSize(hasSize, frame, obj);
	}
}