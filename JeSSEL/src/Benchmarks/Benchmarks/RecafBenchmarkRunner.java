package Benchmarks;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import Benchmarks.BenchmarkData.Benchmark;
import recaf.core.Ref;
import recaf.core.direct.Env;
import recaf.core.direct.EvalJavaStmtNoHOAS;
import recaf.core.direct.IExecEnv;
import recaf.parser.RecafParser;

public class RecafBenchmarkRunner {	
	// Setup the benchmark => Parse the source
	private static IExecEnv parseSource(Benchmark benchmark){		
		try {
			Path path = benchmark.getFilePath();
			File f = path.toFile();
	        FileInputStream in = new FileInputStream(f);
	        return RecafParser.parseJSLCode(in);
		} catch (IOException ex) { // IOException for file
			ex.printStackTrace();
		}
		return null;
	}
	
	private static Env makeEnv(Benchmark benchmark, int runNumber) {
		Env env = null;
		for (int i = 0; i < benchmark.getNrArguments(); i++) {
			String argName = benchmark.getArgumentNames().get(i);
			Integer value = benchmark.getRunValues().get(runNumber*(benchmark.getNrArguments()) + i);
			env = new Env(argName, new Ref<Integer>(value), env);
		}
		return env;
	}
	
	@SuppressWarnings("unused")
	private static void performTestRun(int runNumber, Benchmark benchmark, IExecEnv parsedSource) {
		Env env = makeEnv(benchmark, runNumber);
		try { 
		parsedSource.exec(null, env);
		} catch (Throwable ex) {
			if (ex instanceof EvalJavaStmtNoHOAS.Return) {
				if(((EvalJavaStmtNoHOAS.Return) ex).getValue() instanceof Ref){
					System.out.println("Result of run " + runNumber + " is: " + ((Ref)((EvalJavaStmtNoHOAS.Return) ex).getValue()).value());
				} else {
					System.out.println("Resultof run " + runNumber + " is: " + ((EvalJavaStmtNoHOAS.Return) ex).getValue());
				}
			} else {
				System.out.println("Something went wrong: " + ex.getLocalizedMessage());
				ex.printStackTrace();
			}
		}
	}
	

	// Run the benchmark, write output to file
	// Since Recaf does not have a way of inputting paramaters into
	// the main function, we add them to the Env before we call Exec
	private static List<Long> measure(int runNumber, IExecEnv parsedSource, int warmupRuns, int actualRuns, Benchmark benchmark){
		ArrayList<Long> results = new ArrayList<>();
		Env env;
		
		// Uncomment this to check if benchmark outputs the right result
		//performTestRun(runNumber, benchmark, parsedSource);
		
		// Do the warmup
		for (int i = 0; i < warmupRuns; i++){
			try {
				env = makeEnv(benchmark, runNumber);
				parsedSource.exec(null, env);
			} catch (Throwable ex) {
			}
		}
		
		// Do the actual measurement and store result
		long t1, t2;
		for (int i = 0; i < actualRuns; i++){
			env = makeEnv(benchmark, runNumber);
			t1 = System.nanoTime();
			// The try and catch should be counted with performance, since they are a necessary part of the execution
			try {
				parsedSource.exec(null, env);
			} catch (Throwable ex) {
			}
			t2 = System.nanoTime();
			results.add(t2 - t1);
		}
		return results;
	}

	// Cleanup the setup => Write results to CSV file
	private static void finish(int runNumber, int actualRuns, List<Long> results, BufferedWriter out) {
		// Run1 -> 50 values -> total time
		String[] strArr = new String[actualRuns+2];
		long totalTimeNano = 0;
		strArr[0] = "Input " + String.valueOf(runNumber+1);
		for(int i = 0; i < actualRuns; i++) {
			long run = results.get(i);
			totalTimeNano += run;
			strArr[i+1] = String.valueOf(run);
		}
		
		strArr[actualRuns+1] = String.valueOf(totalTimeNano);
		
		System.out.println("Finished measuring input " + (runNumber+1) + "\nTotal time for " + actualRuns + " runs: " + String.valueOf(totalTimeNano/1000000000.0) + " seconds");
		printLine(out, strArr);

	}
	
	private static void printLine(BufferedWriter f, String... stringArgs){
		int len = stringArgs.length;
		if (len <= 0){
			return;
		}
		try {
			for(int i = 0; i < len-1; i++) {
				f.append(stringArgs[i]).append(',');
			}
			f.append(stringArgs[len-1]).append('\n');
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static Benchmark findBenchmark(String name) {
		for(Benchmark benchmark : BenchmarkData.getBenchmarks()) {
			if (benchmark.getName().equals(name)) {
				return benchmark;
			}
		}
		throw new NoSuchElementException();
	}
	
	
	
	public static void main(String[] args) {
		String name = args[0];
		int runNr = Integer.parseInt(args[1]);
		Benchmark benchmark = findBenchmark(name);
		
		int warmupRuns = Integer.parseInt(args[2]);
		int actualRuns = Integer.parseInt(args[3]);	
		String resultFilePath = args[4];
		
		System.out.println("============= Now running on Recaf: " + benchmark.getName() + " =============");
		try{
			BufferedWriter out = new BufferedWriter(new FileWriter(resultFilePath, true));
			IExecEnv parsed = parseSource(benchmark);
			List<Long> result = measure(runNr, parsed, warmupRuns, actualRuns, benchmark);
			finish(runNr, actualRuns, result, out);
			out.flush();
		} catch (IOException ex) {
			System.err.println("Could not open file");
		}
	}
}
