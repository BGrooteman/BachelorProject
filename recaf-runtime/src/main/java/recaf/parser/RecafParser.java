package recaf.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;

import recaf.core.Ref;
import recaf.core.direct.Env;
import recaf.core.direct.EvalJavaStmtNoHOAS;
import recaf.core.direct.IExec;
import recaf.core.direct.IExecEnv;

public class RecafParser {
	public static void main(String[] args) throws FileNotFoundException {
		runJSLCode(new FileInputStream(new File("test.jsl")));
	}
	
	public static void runJSLCode(FileInputStream in) {
		try {
			CompilationUnit cu = JavaParser.parse(in);
			TranslatorVisitor visitor = new TranslatorVisitor();
			
			IExecEnv result = (IExecEnv) cu.accept(visitor, new Env(null, null, null));
			result.exec(null, new Env("f", new Ref<Integer>(10), null));
		} catch (Throwable ex) {
			if (ex instanceof EvalJavaStmtNoHOAS.Return) {
				if(((EvalJavaStmtNoHOAS.Return) ex).getValue() instanceof Ref){
					System.out.println("Result is: " + ((Ref)((EvalJavaStmtNoHOAS.Return) ex).getValue()).value());
				} else {
					System.out.println("Result is: " + ((EvalJavaStmtNoHOAS.Return) ex).getValue());
				}
			} else {
				System.out.println("Something went wrong: " + ex.getLocalizedMessage());
				ex.printStackTrace();
			}
		}
		
	}
	
	public static IExecEnv parseJSLCode(FileInputStream in) {
		CompilationUnit cu = JavaParser.parse(in);
		TranslatorVisitor visitor = new TranslatorVisitor();
		IExecEnv result = (IExecEnv) cu.accept(visitor, new Env(null, null, null));
		return result;
	}
}