import csv
import numpy as np
import matplotlib.pyplot as plt

def truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])

benchmark_names = ["While", "PrimeFinder", "GCD", "Fibonacci", "Factorial", "FactorCounter"]
total_times_jes = []
total_times_recaf  = []
for name in benchmark_names:
	path_jessel = "./Jessel/" + name + ".csv"
	path_recaf = "./Recaf/" + name + ".csv"
	with open(path_jessel, 'r') as f_j, open (path_recaf, 'r') as f_r:
		jessel_data = list(csv.reader(f_j))
		recaf_data	= list(csv.reader(f_r))

	#One bar graph where we see the relative speedup of JeSSEL
	int_data_jes = []

	list_jes = []
	list_recaf = []

	for i in range(4):
		list_jes.append(int(jessel_data[i+1][-1]))
		list_recaf.append(int(recaf_data[i+1][-1]))

	total_times_jes.append(list_jes)
	total_times_recaf.append(list_recaf)
speedup = []
for (l1, l2) in list(zip(total_times_jes, total_times_recaf)):
	# print(l1 + l2)
	speedup.append([(recaf/jessel) for jessel, recaf in zip(l1, l2)])
	

with open("relativeSpeedup.csv", "w") as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(["", "Input 1", "Input 2", "Input 3", "Input 4"])
        for name, speedupList in zip(benchmark_names, speedup):
            strList = [str(truncate(x, 4)) for x in speedupList]
            writer.writerow([name] + strList)

for benchmark, jtime, rtime in zip(benchmark_names, total_times_jes, total_times_recaf):
	with open("runtimes"+ benchmark + ".csv", "w") as csv_file:
		writer = csv.writer(csv_file, delimiter=',')
		writer.writerow(["", "Input 1", "Input 2", "Input 3", "Input 4"])
		writer.writerow(["JeSSEL"] + list(map(lambda x: truncate(x/1000000000, 4), jtime)))
		writer.writerow(["Recaf"] + list(map(lambda x: truncate(x/1000000000,4), rtime)))

# We want data like this:
# Benchmark 1 => Run 1, speedup of 1.2
# Benchmark 1 => Run 2, speedup of 2
# ============== Plotting ===================
# Setting the positions and width for the bars
pos = np.arange(6)
opacity = 0.8
margin = 0.05
width = (1.-2.*margin)/6

# Plotting the bars
fig, ax = plt.subplots(figsize=(10,5))

colors = ["#FFC222", "green", "blue", "red"]

plotData = []
plotData.append([a[0] for a in speedup])
plotData.append([a[1] for a in speedup])
plotData.append([a[2] for a in speedup])
plotData.append([a[3] for a in speedup])

for i, vals in enumerate(plotData):
	xdata = pos+margin+(i*width)
	plt.bar(xdata,
	        #using df['pre_score'] data,
	        vals,
	        # of width
	        width,
	        # with alpha 0.5
	        alpha=0.5,
	        # with color
	        color=colors[i],
	        # with label the first value in first_name
	        label="Input " + str(i+1))

# Set axis
axes = plt.gca()
axes.set_ylim([0,100])

# Set the position of the x ticks
ax.set_xticks(pos+0.265)
ax.set_xticklabels(benchmark_names)

#Set labels
plt.xlabel('Benchmarks')
plt.ylabel('Speedup (x)')
plt.title('Relative speedup of JeSSEL with respect to Recaf')
plt.legend()
 
plt.tight_layout()
# plt.show()
plt.savefig("relativebars.png")