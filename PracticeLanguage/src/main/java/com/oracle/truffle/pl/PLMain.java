package com.oracle.truffle.pl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.math.BigInteger;

import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.dsl.UnsupportedSpecializationException;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.api.vm.PolyglotEngine;
import com.oracle.truffle.api.vm.PolyglotEngine.Value;
import com.oracle.truffle.pl.runtime.PLFunction;
import com.oracle.truffle.pl.runtime.PLNull;
import com.oracle.truffle.pl.runtime.PLUndefinedNameException;
import com.oracle.truffle.pl.runtime.PLContext;

public final class PLMain {

    public static void main(String[] args) throws IOException {
        Source source;
        if(args.length == 0) {
            System.out.println("No args are supplied");
            source = Source.newBuilder(new InputStreamReader(System.in)).
                    name("<stdin>").
                    mimeType(PLLanguage.MIME_TYPE).
                    build();
        } else {
            source = Source.newBuilder(new File(args[0])).build();
        }

        executeSource(source, System.in, System.out);
    }

    private static void executeSource(Source source, InputStream in, PrintStream out) {
        out.println("== Running on " + Truffle.getRuntime().getName());

        PolyglotEngine engine = PolyglotEngine.newBuilder().setIn(in).setOut(out).build();
        assert engine.getLanguages().containsKey(PLLanguage.MIME_TYPE);

        try {
            Value result = engine.eval(source);

            if (result == null) {
                throw new PLException("No function main() defined in the PL source file");
            } else if (result.get() != PLNull.SINGLETON) {
                out.println(result.get());
            }
        } catch (Throwable ex) {
            Throwable cause = ex.getCause();
            if(cause instanceof UnsupportedSpecializationException) {
                out.println(formatTypeError((UnsupportedSpecializationException) cause));
            } else if (cause instanceof PLUndefinedNameException) {
                out.println(cause.getMessage());
            } else {
                // Unexpected error. Print Stacktrace for debugging
                ex.printStackTrace(out);
            }
        }

        engine.dispose();
    }

    public static String formatTypeError(UnsupportedSpecializationException ex) {
        StringBuilder result = new StringBuilder();
        result.append("Type error");
        if (ex.getNode() != null && ex.getNode().getSourceSection() != null) {
            SourceSection ss = ex.getNode().getSourceSection();
            if (ss != null && ss.isAvailable()) {
                result.append(" at ").append(ss.getSource().getName()).append(" line ").append(ss.getStartLine()).append(" col ").append(ss.getStartColumn());
            }
        }
        result.append(": operation");
        if (ex.getNode() != null) {
            NodeInfo nodeInfo = PLContext.lookupNodeInfo(ex.getNode().getClass());
            if (nodeInfo != null) {
                result.append(" \"").append(nodeInfo.shortName()).append("\"");
            }
        }
        result.append(" not defined for");

        String sep = " ";
        for (int i = 0; i < ex.getSuppliedValues().length; i++) {
            Object value = ex.getSuppliedValues()[i];
            Node node = ex.getSuppliedNodes()[i];
            if (node != null) {
                result.append(sep);
                sep = ", ";

                if (value instanceof Long || value instanceof BigInteger) {
                    result.append("Number ").append(value);
                } else if (value instanceof Boolean) {
                    result.append("Boolean ").append(value);
                } else if (value instanceof String) {
                    result.append("String \"").append(value).append("\"");
                } else if (value instanceof PLFunction) {
                    result.append("Function ").append(value);
                } else if (value == PLNull.SINGLETON) {
                    result.append("NULL");
                } else if (value == null) {
                    // value is not evaluated because of short circuit evaluation
                    result.append("ANY");
                } else {
                    result.append(value);
                }
            }
        }
        return result.toString();
    }

}