package com.oracle.truffle.pl.runtime;

import com.oracle.truffle.api.interop.CanResolve;
import com.oracle.truffle.api.interop.MessageResolution;
import com.oracle.truffle.api.interop.Resolve;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.pl.PLLanguage;

@MessageResolution(receiverType = PLNull.class, language = PLLanguage.class)
public class PLNullMessageResolution {
	
	@Resolve(message = "IS_NULL")
	public abstract static class PLForeignIsNullNode extends Node {
		
		public Object access(Object receiver) {
			return PLNull.SINGLETON == receiver;
		}
	}
	
	@CanResolve
	public abstract static class CheckNull extends Node {
		
		protected static boolean test(TruffleObject receiver) {
			return receiver instanceof PLNull;
		}
	}
}